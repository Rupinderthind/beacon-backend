 const doc={
  openapi: '3.0.1',
  info: {
    version: '1.3.0',
    title: 'Beacon',
    description: 'App to post quotes ',
    termsOfService: 'blank',
    contact: {
      name: 'Mykyta Brazhyskyy',
      email: 'mykyta.brazhynskyy@gmail.com',
      url: 'blank'
    },
    license: {
      name: 'Apache 2.0',
      url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
    }
  },
  servers: [
    {
      url: 'https://transfigureapp.herokuapp.com/',
      description: 'Production server'
    }
  ],
  paths: {
    '/users/': {
      post: {
        tags: ['User'],
        description: 'Get all users',
        operationId: 'getallUsers',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Get list of users'
          },
          '400': {
            description: 'Auth failed',

          }
        }
      }
    },

    '/users/getOne': {
      post: {
        tags: ['User'],
        description: 'Get one user specified by userID',
        operationId: 'getOneUSer',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  userID: {
                    type: 'string',
                    description: 'ID of user we want to get',
                    example: '48fecb34e4bd2a24bae8c025b219b4171811d062'
                }
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Fetched user successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      },
                      user: {
                        type: 'object'
                      },
                      avatar: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true',
                  user:{},
                  avatar:'dadadax2e2x'
                }
              }
            }
          },
          '300': {
            description: "Can't find that user",
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/updateInfo': {
      post: {
        tags: ['User'],
        description: 'Update information fields of user',
        operationId: 'updateUserInfo',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  field: {
                    type: 'string',
                    description: 'Field we want to update',
                    example: 'username'
                },
                value: {
                  type: 'string',
                  description: 'Value of the field',
                  example: 'superman333'
              }
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Updated user successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true',
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/contactus': {
      post: {
        tags: ['User'],
        description: 'Contact with support,writes email to default email and to user',
        operationId: 'contactSupport',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  message: {
                    type: 'string',
                    description: 'Message to support',
                    example: 'hello'
                }
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Writen succesfuly ',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true',
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/deleteAccount': {
      post: {
        tags: ['User'],
        description: 'Delete user and notifications in friends and quotes',
        operationId: 'deleteUser',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  reason: {
                    type: 'string',
                    description: 'Reason to delete account',
                    example: 'I dont know'
                }
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Deleted succesfully',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true'
                }
              }
            }
          },
          '300': {
            description: 'Failed to delete posts',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    
    '/users/getSettingsInfo': {
      post: {
        tags: ['User'],
        description: 'Get information to fields in settingfs',
        operationId: 'getSettingsInfo',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  }
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Fetched info successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      },
                      settings: {
                        type: 'object'
                      }
                    }
                },
                example: {
                  success: 'true',
                  settings:{email:'hello@gmail.com'}
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/updatePassword': {
      post: {
        tags: ['User'],
        description: 'Update password',
        operationId: 'updatePassword',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  oldPassword: {
                    type: 'string',
                    description: 'old password',
                    example: 'testernn'
                },
                newPassword: {
                  type: 'string',
                  description: 'new password',
                  example: 'test'
              }
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Updated password successfully',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true',
                }
              }
            }
          },
          '300': {
            description: 'old password doesnt match ',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      },
                      reason: {
                        type: 'number'
                      }
                    }
                },
                example: {
                  success: 'false',
                  reason: 0
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/updateAvatar': {
      post: {
        tags: ['User'],
        description: 'Update avatar image',
        operationId: 'updateAvatar',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  base64Img: {
                    type: 'string',
                    description: 'Base64 image',
                    example: 'xx33ddasca576x'
                }
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Updated/set avatar successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true',
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/getAvatar': {
      post: {
        tags: ['User'],
        description: 'Get users avatar',
        operationId: 'getAvatar',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  useruuID: {
                    type: 'string',
                    description: 'uuID of user we want to get avatar of ',
                    example: '3093426595be106d935a47757a7809290f38e551'
                }
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Updated/set avatar successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      },
                      avatar: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true',
                  avatar:'ccx323xdjskxx'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/getdisplayName': {
      post: {
        tags: ['User'],
        description: 'Get display name of user',
        operationId: 'getDisplayName',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Fetched display name successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      },
                      displayName: {
                        type: 'String'
                      }
                    }
                },
                example: {
                  success: 'true',
                  displayName:'Adam XYZ'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/getdisplayTitle': {
      post: {
        tags: ['User'],
        description: 'Get display title of user',
        operationId: 'getDisplayName',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Fetched display title successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      },
                      displayTitle: {
                        type: 'String'
                      }
                    }
                },
                example: {
                  success: 'true',
                  displayTitle:'Software engineer'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/getmyBio': {
      post: {
        tags: ['User'],
        description: 'Get user bio',
        operationId: 'getMyBio',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Fetched display name successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      },
                      about: {
                        type: 'String'
                      }
                    }
                },
                example: {
                  success: 'true',
                  about:'I grew up a long time ago.'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/updatedisplayName': {
      post: {
        tags: ['User'],
        description: 'Update display name',
        operationId: 'updateMyName',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  displayName: {
                    type: 'string',
                    description: 'Display name of user',
                    example: 'Adam XYZ'
                },
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Updated display name successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/updatedisplayTitle': {
      post: {
        tags: ['User'],
        description: 'Update display title',
        operationId: 'updateMyTitle',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  displayTitle: {
                    type: 'string',
                    description: 'Display title of user',
                    example: 'Engineer'
                },
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Updated display title successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/updatemyBio': {
      post: {
        tags: ['User'],
        description: 'Update user bio',
        operationId: 'updateUserBio',
        parameters: [],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                    authKey: {
                      type: 'string',
                      description: 'Auth key',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  about: {
                    type: 'string',
                    description: 'Bio of user',
                    example: 'I like big mugs.'
                },
                }
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Updated bio successfuly',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'true'
                }
              }
            }
          },
          '400': {
            description: 'Wrong authkey',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      success: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  success: 'false'
                }
              }
            }
          },
          '500': {
            description: 'Failed to get data from datebase',
            content: {
              'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string'
                      }
                    }
                },
                example: {
                  message: 'failed to load data from datebase'
                }
              }
            }
          }
        }
      }
    },
    '/users/register/': {
        post: {
          tags: ['User'],
          description: 'Register user',
          operationId: 'registerUsers',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      firstName: {
                        type: 'string',
                        description: 'First name',
                        example: 'Mykyta'
                    },
                    lastName: {
                      type: 'string',
                      example: 'Brazhynskyy'
                    },
                    password: {
                      type: 'string',
                      example: '11111111'
                    },
                    email: {
                      type: 'string',
                      example: 'nikita3331@gmail.com'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'New users were created'
            },
            '400': {
              description: 'User already exists parameters',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        sucess: {
                          type: 'string'
                        },
                        reason: {
                          type: 'number'
                        }
                      }
                  },
                  example: {
                    success: 'false',
                    reason: '0'
                  }
                }
              }
            },
            '401': {
                description: 'Failed to fetch from DB',
                content: {
                  'application/json': {
                    schema: {
                      type: 'object',
                      properties: {
                        sucess: {
                          type: 'string'
                        },
                        reason: {
                          type: 'number'
                        }
                      }
                    },
                    example: {
                      success: 'false',
                      reason: '1'
                    }
                  }
                }
              },
              '402': {
                description: 'User is banned',
                content: {
                  'application/json': {
                    schema: {
                      type: 'object',
                      properties: {
                        sucess: {
                          type: 'string'
                        },
                        reason: {
                          type: 'number'
                        }
                      }
                    },
                    example: {
                      success: 'false',
                      reason: '3'
                    }
                  }
                }
              },
              '403': {
                  description: 'First name of last name contains emoji',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        properties: {
                          sucess: {
                            type: 'string'
                          },
                          reason: {
                            type: 'number'
                          }
                        }
                      },
                      example: {
                        success: 'false',
                        reason: '2'
                      }
                    }
                  }
                }
          }
        }
      },
      '/users/login/': {
        post: {
          tags: ['User'],
          description: 'Login user',
          operationId: 'loginUsers',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      email: {
                        type: 'string',
                        description: 'Email of user who pressed link on email',
                        example: 'nikita3331@gmail.com'
                    },
                    password: {
                      type: 'string',
                      example: 'tester'
                    },

                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'User login successful',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        sucess: {
                          type: 'string'
                        },
                        authKey: {
                          type: 'string'
                        }
                        ,
                        uuID: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true',
                    authKey: '87ab191f339fa1f8d44be1869bdfbb659c342bf9',
                    uuID: '3093426595be106d935a47757a7809290f38e551'
                  }
                }
              }
            },
            '400': {
              description: 'User did not press link on email',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        sucess: {
                          type: 'string'
                        },
                        reason: {
                          type: 'number'
                        }
                      }
                  },
                  example: {
                    success: 'false',
                    reason: '0'
                  }
                }
              }
            },
            '401': {
                description: 'Wrong email or password',
                content: {
                  'application/json': {
                    schema: {
                      type: 'object',
                      properties: {
                        sucess: {
                          type: 'string'
                        },
                        reason: {
                          type: 'number'
                        }
                      }
                    },
                    example: {
                      success: 'false',
                      reason: '1'
                    }
                  }
                }
              }
          }
        }
      },
      '/friends/': {
        post: {
          tags: ['Friends'],
          description: 'Get all friends of user',
          operationId: 'alluserFriends',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },

                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'List of users friends',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        friends: {
                          type: 'array'
                        }
                      }
                  },
                  example: {
                    friends: []
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from DB or authkey not valid',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'Failed to load fromd datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/friends/add/': {
        post: {
          tags: ['Friends'],
          description: 'Add new friend based on user uuID',
          operationId: 'addFriends',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    uuID: {
                      type: 'string',
                      description: 'UuID of user to add to friends',
                      example: '48fecb34e4bd2a24bae8c025b219b4171811d062'
                  },
                    

                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Sent user request to friends',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'array'
                        }
                      }
                  },
                  example: {
                    success: 'True'
                  }
                }
              }
            },
            '400': {
              description: 'User already requested or added to friends',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'False'
                  }
                }
              }
            }
          }
        }
      },

      '/friends/accept/': {
        post: {
          tags: ['Friends'],
          description: 'Accept invitation to friends',
          operationId: 'addFriends',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '435072e9cc786e969f40c2f6423e398e8b94c093'
                    },
                    frienduuID: {
                      type: 'string',
                      description: 'UuID of user who added us to friends',
                      example: '3093426595be106d935a47757a7809290f38e551'
                  },
                    

                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successfuly accepted friend',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'array'
                        },
                        accepted: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'True',
                    accepted:'Nikita Brazhynskyy'
                  }
                }
              }
            },
            '500': {
              description: 'Problem with datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'Failed to load from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/friends/decline/': {
        post: {
          tags: ['Friends'],
          description: 'Decline invitation to friends',
          operationId: 'declineFriend',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '435072e9cc786e969f40c2f6423e398e8b94c093'
                    },
                    frienduuID: {
                      type: 'string',
                      description: 'UuID of user who added us to friends',
                      example: '3093426595be106d935a47757a7809290f38e551'
                  },
                    

                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successfuly accepted friend',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'True'
                  }
                }
              }
            },
            '500': {
              description: 'Problem with datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'Failed to load from datebase'
                  }
                }
              }
            }
          }
        }
      },
      

      '/friends/delete/': {
        post: {
          tags: ['Friends'],
          description: 'Delete from list of friends ',
          operationId: 'deleteriend',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    frienduuID: {
                      type: 'string',
                      description: 'UuID of user who we want to delete',
                      example: '48fecb34e4bd2a24bae8c025b219b4171811d062'
                  },
                    

                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successfuly deleted friend',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'array'
                        },
                        deleted: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'True',
                    deleted:'3093426595be106d935a47757a7809290f38e551'
                  }
                }
              }
            },
            '500': {
              description: 'Problem with datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'Failed to load from datebase'
                  }
                }
              }
            }
          }
        }
      },


      '/quotes/': {
        post: {
          tags: ['Quote'],
          description: 'Get all quotes ',
          operationId: 'getQuotes',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'List of quotes',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        quotes: {
                          type: 'array'
                        },
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    quotes: [],
                    success:'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/quotes/getNumUserQuotes': {
        post: {
          tags: ['Quote'],
          description: 'Get all user quotes with date added and quoteUUID ',
          operationId: 'getspecificQuotes',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    uuID: {
                      type: 'string',
                      description: 'UUID of user we want to get quotes',
                      example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                  },
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'List of quotes with date added and quoteID',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        quotes: {
                          type: 'array'
                        },
                        success: {
                          type: 'string'
                        },
                        buildsLength: {
                          type: 'number'
                        },
                        quotesLength: {
                          type: 'number'
                        }
                      }
                  },
                  example: {
                    quotes: [],
                    buildsLength: 10,
                    quotesLength: 10,
                    success:'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/quotes/deleteCommentOfQuote': {
        post: {
          tags: ['Quote'],
          description: 'Delete comment under the quote',
          operationId: 'deleteCommentUnderQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote where comment is',
                      example: 'not defined'
                  },
                  commentID: {
                    type: 'string',
                    description: 'ID of comment we want to delete',
                    example: 'not defined'
                },
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful delete',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success:'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/quotes/deleteCommentOfComment': {
        post: {
          tags: ['Quote'],
          description: 'Delete comment under the comment',
          operationId: 'deleteCommentUnderComment',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote where comment is',
                      example: 'not defined'
                  },
                  commentID: {
                    type: 'string',
                    description: 'ID of comment we want to delete',
                    example: 'not defined'
                },
                replyID: {
                  type: 'string',
                  description: 'ID of reply we want to delete',
                  example: 'not defined'
              },
              addedByUuid: {
                type: 'string',
                description: 'User who posted comment',
                example: 'not defined'
            },
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful delete',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success:'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      
      '/quotes/addtofavorites': {
        post: {
          tags: ['Quote'],
          description: 'Add quote to user favorites ,to use in screen later.',
          operationId: 'addToFavorites',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote we want to add',
                      example: 'none'
                  },
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Added/removed from favorites',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success:'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/quotes/getrecentlySearched': {
        post: {
          tags: ['Quote'],
          description: 'Get the array of recently searched keyword ',
          operationId: 'getRecentlySearched',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'List of recently searched keyword',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        recentlySearched: {
                          type: 'array'
                        },
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    recentlySearched: [],
                    success:'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/quotes/updaterecentlySearched': {
        post: {
          tags: ['Quote'],
          description: 'Push new word to recently searched and delete old one.Max 5 keyword.',
          operationId: 'updateRecentlySearched',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    searchWord: {
                      type: 'string',
                      description: 'Our search word',
                      example: 'football'
                  },
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Added new word',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success:'true'
                  }
                }
              }
            },
            '300': {
              description: 'Word already on list',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success:'false'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/quotes/deleterecentlySearched': {
        post: {
          tags: ['Quote'],
          description: 'Delete given keyword',
          operationId: 'deleteRecentlySearched',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    deleteWord: {
                      type: 'string',
                      description: 'Our search word to delete',
                      example: 'football'
                  },
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Deleted word ',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success:'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/quotes/getOne/': {
        post: {
          tags: ['Quote'],
          description: 'Get one quote specified by quote uuID ',
          operationId: 'getoneQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'Uuid of qoute.We get it from notification ',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Returns one qoute',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        quote: {
                          type: 'object'
                        }
                      }
                  },
                  example: {
                    quote: {}
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        quote: {
                          type: 'object'
                        }
                      }
                  },
                  example: {
                    quote: {}
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/quotes/getRandomQuote/': {
        post: {
          tags: ['Quote'],
          description: 'Get one random quote matching expression,with skip ',
          operationId: 'getRandomQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    offset: {
                      type: 'number',
                      description: 'Offset of filtered quotes ',
                      example: '1'
                  },
                  searchWord: {
                    type: 'string',
                    description: 'Word we want to search ',
                    example: 'football'
                }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Returns one qoute',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        quote: {
                          type: 'object'
                        },
                        favorite: {
                          type: 'string'
                        },
                        currentUser: {
                          type: 'object'
                        },
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    quote: {},
                    currentUser:{},
                    success:'true',
                    favorite:'true'
                  }
                }
              }
            },
            '300': {
              description: 'Failed to fetch quote',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success:'false'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        },
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase',
                    success:'false'
                  }
                }
              }
            }
          }
        }
      },

      '/quotes/getUserQuotes/': {
        post: {
          tags: ['Quote'],
          description: 'Get one quote specified by quote uuID ',
          operationId: 'getoneQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    addedByUuid: {
                      type: 'string',
                      description: 'Uuid of user who added quote.Needed for viewing all users quotes in profile. ',
                      example: '3093426595be106d935a47757a7809290f38e551'
                  }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Returns one qoute',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        quotes: {
                          type: 'array'
                        }
                      }
                  },
                  example: {
                    quotes: []
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        quotes: {
                          type: 'array'
                        }
                      }
                  },
                  example: {
                    quotes: []
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },


      '/quotes/delete/': {
        post: {
          tags: ['Quote'],
          description: 'Delets one quote.User can delete only his quote based on authkey',
          operationId: 'getoneQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'Quote uuID to be deleted. ',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Deleted successfuly',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/quotes/add/': {
        post: {
          tags: ['Quote'],
          description: 'Adds one quote by user',
          operationId: 'addoneQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    title: {
                      type: 'string',
                      description: 'Quote title,one of the random ones we get',
                      example: 'I achieved'
                  },
                  image: {
                    type: 'string',
                    description: 'Image link for qoute.Then here will be added uploading own images',
                    example: 'www.google.com'
                },
                style: {
                  type: 'object',
                  description: 'Styles of our qoute',
                  example: {'font':'bold','backgroundColor':'red','fontSize':25}
              },
              quoteText: {
                type: 'string',
                description: 'Text of our quote',
                example: 'I achieved many good things today'
            },
            shareWith: {
              type: 'number',
              description: 'Who do we want to share with 0-everyone -1 friends -2 me',
              example: 'I achieved many good things today'
          }     
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Added successfuly',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },


      '/notifications/': {
        post: {
          tags: ['Notiications'],
          description: 'Gets all notifications for user.Right now there is 0-friend request 1- quote added by friend -2 user posted comment under quote -3 user replied to our comment -4 someone liked our photo -5 someone liked our comment ',
          operationId: 'getallNotifications',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Deleted successfuly',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        notifications: {
                          type: 'array'
                        }
                      }
                  },
                  example: {
                    success: 'true',
                    notifications:[]
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/notifications/getAllPosts': {
        post: {
          tags: ['Notiications'],
          description: 'Gets all notifications for user for all posts.Right now there is 0-friend request 1- quote added by friend -2 user posted comment under quote -3 user replied to our comment -4 someone liked our photo -5 someone liked our comment ',
          operationId: 'getallPostsNotifications',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Fetched successfuly',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        notifications: {
                          type: 'array'
                        }
                      }
                  },
                  example: {
                    success: 'true',
                    notifications:[]
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/notifications/getMyPosts': {
        post: {
          tags: ['Notiications'],
          description: 'Gets all notifications for user for my posts.Right now there is 0-friend request 1- quote added by friend -2 user posted comment under quote -3 user replied to our comment -4 someone liked our photo -5 someone liked our comment ',
          operationId: 'getmyPostsNotifications',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Fetched successfuly',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        notifications: {
                          type: 'array'
                        }
                      }
                  },
                  example: {
                    success: 'true',
                    notifications:[]
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/notifications/getlengtheverything': {
        post: {
          tags: ['Notiications'],
          description: 'Get length of all notifications',
          operationId: 'getNotifLength',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Fetched successfuly',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        notificationslength: {
                          type: 'number'
                        }
                      }
                  },
                  example: {
                    success: 'true',
                    notificationslength:2
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/notifications/delete/': {
        post: {
          tags: ['Notiications'],
          description: 'Delete notification.For example after we add friend and we see that information.',
          operationId: 'deleteNotification',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '435072e9cc786e969f40c2f6423e398e8b94c093'
                    },
                    notificationID: {
                      type: 'string',
                      description: 'UuID of notification to be deleted',
                      example: 'de3a559dbe0cf5d2c55025ea6c1ddc80a3df2259'
                  }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Deleted successfuly',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        deleted: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true',
                    deleted:'de3a559dbe0cf5d2c55025ea6c1ddc80a3df2259'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/quotes/addComment/': {
        post: {
          tags: ['Quote'],
          description: 'Comment post ',
          operationId: 'commentPost',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote we want to comment',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  },
                  commentText: {
                    type: 'string',
                    description: 'Text of comment we want to post',
                    example: 'Great quote .Good luck!'
                },
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Posted successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },


      '/quotes/replytocomment/': {
        post: {
          tags: ['Quote'],
          description: 'Reply to comment under post  ',
          operationId: 'replyToComment',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote we want to comment',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  },
                  replyText: {
                    type: 'string',
                    description: 'Text of reply we want to post',
                    example: 'Great quote .Good luck!'
                },
                commentID: {
                  type: 'string',
                  description: 'ID of comment we want to comment',
                  example: 'fbdf965bfc4fd5c6aa57e8357f8e8556ba55a8ed'
              },
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Posted successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },


      '/quotes/like/': {
        post: {
          tags: ['Quote'],
          description: 'Like quote ',
          operationId: 'likeQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote we want to like',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  }
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Liked successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '300': {
              description: 'User already liked our quote',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/quotes/dislike/': {
        post: {
          tags: ['Quote'],
          description: 'Dis quote ',
          operationId: 'dislikeQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote we want to like',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  }
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Disliked successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '300': {
              description: 'User doesn"t like this quote',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },


      '/quotes/likeComment/': {
        post: {
          tags: ['Quote'],
          description: 'Like comment ',
          operationId: 'likeComment',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote where the comment is',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  },
                  commentID: {
                    type: 'string',
                    description: 'ID of comment we want to like',
                    example: 'fbdf965bfc4fd5c6aa57e8357f8e8556ba55a8ed'
                },
                  
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Liked successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/quotes/dislikeComment/': {
        post: {
          tags: ['Quote'],
          description: 'Dislike comment ',
          operationId: 'dislikeComment',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote where the comment is',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  },
                  commentID: {
                    type: 'string',
                    description: 'ID of comment we want to dislike',
                    example: 'fbdf965bfc4fd5c6aa57e8357f8e8556ba55a8ed'
                },
                  
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Liked successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },

      '/reports/quote/': {
        post: {
          tags: ['Report'],
          description: 'Report quote  ',
          operationId: 'reportQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote which we want to report',
                      example: 'c4c76568b9569c16526614bb3469b08bfd6b03f8'
                  },
                  reason: {
                    type: 'string',
                    description: 'The reason we report this',
                    example: 'But I must explain to you how all this'
                },
                  
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Reported successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reports/user/': {
        post: {
          tags: ['Report'],
          description: 'Report user  ',
          operationId: 'reportUser',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    userID: {
                      type: 'string',
                      description: 'ID of user who we want to report',
                      example: '3093426595be106d935a47757a7809290f38e551'
                  },
                  reason: {
                    type: 'string',
                    description: 'The reason we report this',
                    example: 'But I must explain to you how all this'
                },
                  
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Reported successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reports/reflection/': {
        post: {
          tags: ['Report'],
          description: 'Report reflection  ',
          operationId: 'reportReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    reflectionID: {
                      type: 'string',
                      description: 'ID of reflection we want to report',
                      example: '3093426595be106d935a47757a7809290f38e551'
                  },
                  reason: {
                    type: 'string',
                    description: 'The reason we report this',
                    example: 'But I must explain to you how all this'
                },
                  
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Reported successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '400': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/uploadReflection/': {
        post: {
          tags: ['Admin'],
          description: 'Add new reflection  ',
          operationId: 'addReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                  base64Img: {
                    type: 'string',
                    description: 'Base64 string of image',
                    example: 'xnncbeeuu22888zxnaaw'
                },
                fontColor: {
                  type: 'string',
                  description: 'Font color of text on refleciton',
                  example: 'white'
              },
                
                reflectionText: {
                  type: 'string',
                  description: 'Text of reflection',
                  example: 'What are the two moments in your life that you never forget?'
              },
                  
                
                  
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Uploaded successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '207': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reflections/getReflection/': {
        post: {
          tags: ['Reflections'],
          description: 'Get reflection  ',
          operationId: 'getReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Got successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        reflection: {
                          type: 'Object'
                        },
                        
                      }
                  },
                  example: {
                    success: 'true',
                    reflection:{}
                  }
                }
              }
            },
            '300': {
              description: "Reflection doesn't exist",
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'false',
                  }
                }
              }
            },
            '400': {
              description: 'Wrong login/password',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reflections/like/': {
        post: {
          tags: ['Reflections'],
          description: 'Like reflection  ',
          operationId: 'likeReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Liked successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'true',
                  }
                }
              }
            },
            '300': {
              description: "Reflection doesn't exist",
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'false',
                  }
                }
              }
            },
            '400': {
              description: 'Wrong login/password',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reflections/dislike/': {
        post: {
          tags: ['Reflections'],
          description: 'Dislike reflection  ',
          operationId: 'dislikeReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    }
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Disliked successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'true',
                  }
                }
              }
            },
            '300': {
              description: "Reflection doesn't exist",
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'false',
                  }
                }
              }
            },
            '400': {
              description: 'Wrong login/password',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reflections/addComment/': {
        post: {
          tags: ['Reflections'],
          description: 'Add comment to  reflection  ',
          operationId: 'addCommentReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    commentText: {
                      type: 'string',
                      description: 'Comment we want to add',
                      example: 'I also feel the same way'
                  },
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Commented successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'true',
                  }
                }
              }
            },
            '300': {
              description: "Reflection doesn't exist",
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'false',
                  }
                }
              }
            },
            '400': {
              description: 'Wrong login/password',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reflections/replyToComment/': {
        post: {
          tags: ['Reflections'],
          description: 'Reply to users comment   ',
          operationId: 'addCommenttoCommentReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                    replyText: {
                      type: 'string',
                      description: 'Comment  we want to add to comment',
                      example: 'I really like your comment'
                  },
                  commentID: {
                    type: 'string',
                    description: 'ID of comment we want to reply',
                    example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                },
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Replied successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'true',
                  }
                }
              }
            },
            '300': {
              description: "Reflection doesn't exist",
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'false',
                  }
                }
              }
            },
            '400': {
              description: 'Wrong login/password',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reflections/likeComment/': {
        post: {
          tags: ['Reflections'],
          description: 'Like users comment',
          operationId: 'likeCommentReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                  commentID: {
                    type: 'string',
                    description: 'ID of comment we want to like',
                    example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                },
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Liked comment successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'true',
                  }
                }
              }
            },
            '300': {
              description: "Reflection doesn't exist",
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'false',
                  }
                }
              }
            },
            '400': {
              description: 'Wrong login/password',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/reflections/dislikeComment/': {
        post: {
          tags: ['Reflections'],
          description: 'Dislike users comment',
          operationId: 'dislikeCommentReflection',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Authkey of user',
                        example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                    },
                  commentID: {
                    type: 'string',
                    description: 'ID of comment we want to dislike',
                    example: '87ab191f339fa1f8d44be1869bdfbb659c342bf9'
                },
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Disliked comment successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'true',
                  }
                }
              }
            },
            '300': {
              description: "Reflection doesn't exist",
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },                        
                      }
                  },
                  example: {
                    success: 'false',
                  }
                }
              }
            },
            '400': {
              description: 'Wrong login/password',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/login/': {
        post: {
          tags: ['Admin'],
          description: 'Login as admin',
          operationId: 'adminLogin',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      login: {
                        type: 'string',
                        description: 'Login of admin',
                        example: 'admin'
                    },
                  password: {
                    type: 'string',
                    description: 'Password of admin',
                    example: 'admin'
                },
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Disliked comment successfully',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }, 
                        authKey: {
                          type: 'string'
                        }                       
                      }
                  },
                  example: {
                    success: 'true',
                    authKey:'not defined'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong login/password',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/getAllUsers/': {
        post: {
          tags: ['Admin'],
          description: 'Get list of all users',
          operationId: 'getAllUsers',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    }
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful fetch',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }, 
                        users: {
                          type: 'Array'
                        }                       
                      }
                  },
                  example: {
                    success: 'true',
                    users:[]
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/getAllReports/': {
        post: {
          tags: ['Admin'],
          description: 'Get list of all reports',
          operationId: 'getAllReports',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    }
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful fetch',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }, 
                        reports: {
                          type: 'Array'
                        }                       
                      }
                  },
                  example: {
                    success: 'true',
                    reports:[]
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/deleteReport/': {
        post: {
          tags: ['Admin'],
          description: 'Delete given report',
          operationId: 'deleteReport',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    reportID: {
                      type: 'string',
                      description: 'ID of report',
                      example: 'not defined'
                  }
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful delete',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }                    
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/deleteQuote/': {
        post: {
          tags: ['Admin'],
          description: 'Delete quote and all of its reports',
          operationId: 'deleteQuote',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    quoteID: {
                      type: 'string',
                      description: 'ID of quote',
                      example: 'not defined'
                  }
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful delete',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }                    
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/deleteUser/': {
        post: {
          tags: ['Admin'],
          description: 'Delete user and all of his quotes',
          operationId: 'deleteUser',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    userID: {
                      type: 'string',
                      description: 'ID of user to be deleted',
                      example: 'not defined'
                  }
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful delete of user',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }                    
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '205': {
              description: 'Error in delete user ',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/messageUser/': {
        post: {
          tags: ['Admin'],
          description: 'Send email to user',
          operationId: 'emailUser',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    reportID: {
                      type: 'string',
                      description: 'ID of report we want to respond to user',
                      example: 'not defined'
                  },
                  messageContent: {
                    type: 'string',
                    description: 'Message we want to send to user',
                    example: 'hello'
                }
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful delete of user',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }                    
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '205': {
              description: 'Error in delete user ',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },
            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/getSupportContacts/': {
        post: {
          tags: ['Admin'],
          description: 'Get support contacts',
          operationId: 'supportContacts',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    }
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful fetch',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        contacts: {
                          type: 'Array'
                        }        
                                        
                      }
                  },
                  example: {
                    success: 'true',
                    contacts:[]
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },

            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/deleteContact/': {
        post: {
          tags: ['Admin'],
          description: 'Delete one user contact',
          operationId: 'deleteContact',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    contactID: {
                      type: 'string',
                      description: 'ID of contact',
                      example: 'not defined'
                  }
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful delete',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }      
                                        
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },

            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/respondToContact/': {
        post: {
          tags: ['Admin'],
          description: 'Respond to user contact,then talk by email',
          operationId: 'respondToContact',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    userEmail: {
                      type: 'string',
                      description: 'Email of user we want to contact',
                      example: 'xyz@gmail.com'
                  },
                  messageToUser: {
                    type: 'string',
                    description: 'Message we want to send to user',
                    example: 'message'
                },
                contactID: {
                  type: 'string',
                  description: 'ID of contact to be deleted',
                  example: 'not defined'
              },
                
                  
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful sent message',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }      
                                        
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },

            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/messageUserUuid/': {
        post: {
          tags: ['Admin'],
          description: 'Respond to user contact,then talk by email',
          operationId: 'respondToContact',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    uuID: {
                      type: 'string',
                      description: 'UUID of user',
                      example: 'xsss222'
                  },
                  messageContent: {
                    type: 'string',
                    description: 'Message we want to send to user',
                    example: 'message'
                }
                
                  
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful sent message',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }      
                                        
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },

            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/getAllUserQuotes/': {
        post: {
          tags: ['Admin'],
          description: 'Get all quotes posted by user',
          operationId: 'getAllQuotes',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    uuID: {
                      type: 'string',
                      description: 'UUID of user',
                      example: 'xsss222'
                  }
                
                  
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful fetch quotes',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        quotes: {
                          type: 'array'
                        }        
                                        
                      }
                  },
                  example: {
                    success: 'true',
                    quotes:[]
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },

            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/sendMessageToAll/': {
        post: {
          tags: ['Admin'],
          description: 'Send message to all users',
          operationId: 'sendToAll',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    message: {
                      type: 'string',
                      description: 'Message to all users',
                      example: 'hello'
                  }
                
                  
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Successful send',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }   
                                        
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },

            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/banEmail/': {
        post: {
          tags: ['Admin'],
          description: 'Ban users email and later remove him',
          operationId: 'banEmail',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    },
                    email: {
                      type: 'string',
                      description: 'Email of user',
                      example: 'hello@xyz.com'
                  }
                
                  
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'Band successful ',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }   
                                        
                      }
                  },
                  example: {
                    success: 'true'
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },

            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
      '/admin/getBanned/': {
        post: {
          tags: ['Admin'],
          description: 'Get list of banned users',
          operationId: 'getbanned',
          parameters: [],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  properties: {
                      authKey: {
                        type: 'string',
                        description: 'Admin authkey',
                        example: 'not defined'
                    }
                
                  
                  
                    
                  }
                }
              }
            },
            required: true
          },
          responses: {
            '200': {
              description: 'fetched successful ',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        },
                        banned: {
                          type: 'array'
                        }     
                                        
                      }
                  },
                  example: {
                    success: 'true',
                    banned:[]
                  }
                }
              }
            },
            '203': {
              description: 'Wrong authkey',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        success: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    success: 'false'
                  }
                }
              }
            },

            '500': {
              description: 'Failed to get data from datebase',
              content: {
                'application/json': {
                  schema: {
                      type: 'object',
                      properties: {
                        message: {
                          type: 'string'
                        }
                      }
                  },
                  example: {
                    message: 'failed to load data from datebase'
                  }
                }
              }
            }
          }
        }
      },
  },
     
tags: [
  {
    name: 'User'
  },
  {
      name: 'Quote'
    },
    {
      name: 'Notiications'
    },
    {
      name: 'Friends'
    },
    {
      name: 'Report'
    },
    {
      name: 'Reflections'
    },
    {
      name: 'Admin'
    }
],
  

};
exports.doc=doc;