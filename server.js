const swaggerUi = require('swagger-ui-express');
const openApiDocumentation = require('./swag/documentation');  
const express = require('express')
const app = express()
const crypto = require("crypto");
const cors = require('cors')
const mongoose = require('mongoose')
const userRouter = require('./routes/users')
const friendRouter = require('./routes/friends')
const notificationRouter = require('./routes/notifications')
const quotesRouter = require('./routes/quotes')
const reportsRouter = require('./routes/reports')
const reflectionsRouter = require('./routes/reflections')
const adminRouter = require('./routes/admin')
require('dotenv').config()

const bodyParser = require('body-parser')
const path = require('path');
const User = require('./models/User');
var cron = require('node-cron');
var gcm = require('node-gcm');
var sender = new gcm.Sender(process.env.FCM_SERVER_KEY);

//heroku pass N1111111!

let db_uri = process.env.MONGODB_URI
console.log(process.env,db_uri,"db_uri")
mongoose.connect(db_uri, { useNewUrlParser: true,useUnifiedTopology: true })



mongoose.connection.on('error', (error) => console.error(error))
mongoose.connection.once('open', () => console.log('Connected to Database'))

app.use(bodyParser.json({
    limit: '50mb',
    extended: true
  }));
  
  app.use(bodyParser.urlencoded({
    limit: '50mb',
    parameterLimit: 500000,
    extended: true 
  }));

app.use(cors())
app.use(express.static(path.join(__dirname, 'client/build')));
app.get('/admin', (req, res) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openApiDocumentation.doc));
app.use('/users', userRouter)
app.use('/friends', friendRouter)
app.use('/notifications', notificationRouter)
app.use('/quotes', quotesRouter)
app.use('/reports', reportsRouter)
app.use('/reflections', reflectionsRouter)
app.use('/admin', adminRouter)

setInterval(async () => {
  let users = await User.find({'confirmed':true, notificationEnabled: true},{email:1, uuID: 1})
  if(users.length) {
    users.forEach(async user => {
      let notification={"action": 8, "name": "Team Transfigure",'title':"Hi How are you today?",
          'addedAt':Date.now(), 'uuID': user.uuID}
      await User.updateOne({_id: user._id}, { $push: { notifications: notification } })
    })
  }
}, 1000*60*60*24)

cron.schedule('0 17 * * 2', async () => {
  let users = await User.find({'confirmed':true})
  if(users.length) {
    users.forEach(async user => {
      var message = new gcm.Message({
        priority: 'high',
        contentAvailable: true,
        notification: {
          title: "Transfigure",
          icon: "./playstore.png",
          body: `Hey how is your day going?`
        }
      });
      sender.send(message, { registrationTokens: [user.deviceHash] }, function (err, response) {
        if (err) console.error(err,"sdsdasdsdsdasdsd");
        else console.log(response,"responsenotification");
      });
    })
  }
});

cron.schedule('0 17 * * 4', async () => {
  let users = await User.find({'confirmed':true})
  if(users.length) {
    users.forEach(async user => {
      var message = new gcm.Message({
        priority: 'high',
        contentAvailable: true,
        notification: {
          title: "Transfigure",
          icon: "./playstore.png",
          body: `Anything new with you`
        }
      });
      sender.send(message, { registrationTokens: [user.deviceHash] }, function (err, response) {
        if (err) console.error(err,"sdsdasdsdsdasdsd");
        else console.log(response,"responsenotification");
      });
    })
  }
});

cron.schedule('0 17 * * 6', async () => {
  let users = await User.find({'confirmed':true})
  if(users.length) {
    users.forEach(async user => {
      var message = new gcm.Message({
        priority: 'high',
        contentAvailable: true,
        notification: {
          title: "Transfigure",
          icon: "./playstore.png",
          body: `Let us know how your doing today!`
        }
      });
      sender.send(message, { registrationTokens: [user.deviceHash] }, function (err, response) {
        if (err) console.error(err,"sdsdasdsdsdasdsd");
        else console.log(response,"responsenotification");
      });
    })
  }
});

app.listen(process.env.PORT||3000, () => console.log('Server Started'))

