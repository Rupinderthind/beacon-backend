import React from 'react';
import Header from '../components/header'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateKey} from '../actions/user'
import { getSupportContacts,deleteContact,respondToContact} from '../func/admin'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import Alert from 'react-bootstrap/Alert'
import Popup from "reactjs-popup";



import './styles/reports.css';

class Support extends React.Component {
  constructor(props) {
    super(props);
    this.state={Login:'admin',Password:'admin',contacts:null,searchWord:'',showAlert:false,messageoverlayvis:false,messageToUser:'',chosenItem:'',chosenTag:''}
  }

async componentDidMount(){
    await this.fetchContacts()
}
async fetchContacts(){
    let resp=await getSupportContacts(this.props.user.authKey)
    if(resp.success){
        let obj=0
        let newreports=[]
        for(let i=0;i<resp.contacts.length;i++){
            obj={...resp.contacts[i],pressed:false}
            newreports.push(obj)
        }
        this.setState({contacts:newreports,showAlert:false})
    }
}

renderRow(item,idx){

    return(
      <React.Fragment key={idx}>
        <tr >
            <td>{idx}</td>
            <td>{item.message}</td>
            <td><Button variant="primary" style={{display:'inline-block'}} onClick={()=>{this.deleteContact(item)}}>DELETE</Button></td>
            <td><Button variant="success" style={{display:'inline-block'}} onClick={()=>{this.setState({messageoverlayvis:true,chosenItem:item})}}>RESPOND</Button></td>
        </tr>

        </React.Fragment>

    )
}
async deleteContact(item){

    let resp=await deleteContact(this.props.user.authKey,item._id)
    if(resp.success){
        await this.fetchContacts()
    }
}

async writeToUser(){
  let resp=await respondToContact(this.props.user.authKey,this.state.chosenItem.email,this.state.messageToUser,this.state.chosenItem._id)
  if(resp.success){
    await this.fetchContacts()
  }
  else{
    this.setState({showAlert:true})
  }
  this.setState({messageoverlayvis:false});
}





showOverlay(){
return(
  <Popup open={this.state.messageoverlayvis} onClose={()=>{ this.setState({messageoverlayvis:false,messageToUser:''}) }} modal>
    {close=>(
      <>
    <div>Write message to user</div>
    <FormControl as="textarea" type="email" placeholder="Write your message here" onInput={(e)=>{this.setState({messageToUser:e.target.value})}}/> 
    
    <Button variant="danger"  style={{display:'inline-block',margin:'1vw'}} onClick={()=>{ this.setState({messageoverlayvis:false});close() }}>CLOSE</Button>
    <Button variant="success"  style={{display:'inline-block',margin:'1vw'}} onClick={()=>{ this.writeToUser() }}>SEND</Button>

    </>
    )}

  </Popup>
)
}
showAlert(){
    
  return(
    <Alert show={this.state.showAlert} variant="danger" onClose={() => this.setState({showAlert:false})} dismissible>
    <Alert.Heading>Something went wrong!</Alert.Heading>
    <p>
We were not able to fetch from server
    </p>
    <hr />
  </Alert>
  )
}

  renderList(){
    if(this.state.contacts){
        return(
<Table striped bordered hover >
  <thead>
    <tr>
      <th>#</th>
      <th>Message</th>
      <th>DELETE</th>
      <th>RESPOND</th>
    </tr>
  </thead>
  <tbody>
    
    {this.state.contacts.map((item,idx)=>{return(this.renderRow(item,idx))})}
    

  </tbody>
</Table>
        )
}
else{return(null)}
}




  render() {
    return (
        <div>
      <Header route={'Support'}/>
      {this.showOverlay()}
      {this.showAlert()}
      {this.renderList()}
           </div> 

      
    );
  }

}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateKey }, dispatch)
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Support)

