import React from 'react';
import Header from '../components/header'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateKey,updateCurrentUser,updateUuid} from '../actions/user'
import { getAllUsers,deleteReport,deleteUser,messageUserUuid,sendMessageToAll,banEmail} from '../func/admin'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import Popup from "reactjs-popup";
import Alert from 'react-bootstrap/Alert'
import FormControl from 'react-bootstrap/FormControl'


import './styles/users.css';

class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state={Login:'admin',Password:'admin',users:null,searchWord:'',messageoverlayvis:false,chosenUser:null,messageToUser:'',showAlert:false,usersCopy:null,messageToAll:''}
    
  }

async componentDidMount(){
    await this.fetchUsers()
    await this.props.updateCurrentUser(null)
  await this.props.updateUuid(null)
}
async fetchUsers(){
    let resp=await getAllUsers(this.props.user.authKey)
    if(resp.success){
        let obj=0
        let newusers=[]
        for(let i=0;i<resp.users.length;i++){
            obj={...resp.users[i],pressedList:false,pressedActions:false}
            newusers.push(obj)
        }
        this.setState({users:newusers,usersCopy:newusers})
    }
}
async deteleItem(item){


    let resp=await deleteReport(this.props.user.authKey,item._id)
    if(resp.success){
        await this.fetchUsers()
    }

}
showReport(item,idx){
  let tag='null'
  if('userID' in item){
    tag='USER'
  }
  if('quoteID' in item){
    tag='QUOTE'
  }
  if('deleteAccount' in item){
    tag='DELETE ACCOUNT'
  }
  return(
    <tr style={{backgroundColor:'#0c56eb'}} key={idx}>
    <td style={{backgroundColor:'white'}}> </td>
    <td>{idx}</td>
    <td>
      <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
        {item.reason}
      </div>
    </td>
    <td >
      <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
      {tag} 
      </div>
    </td>
    <td>
      <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
        <Button variant="primary" style={{display:'inline-block'}} onClick={()=>{this.deteleItem(item)}}>DELETE REPORT</Button>
      </div>
    </td>
    <td style={{backgroundColor:'white'}}> </td>
    <td style={{backgroundColor:'white'}}> </td>
    <td style={{backgroundColor:'white'}}> </td>
  </tr>
  )
}
showListOfReports(item,idx){
  if(item.pressedList){
    if(item.reports.length>0){
    
    return(


     

  <>
    <tr style={{backgroundColor:'#0c56eb'}} key={idx}>
      <td style={{backgroundColor:'white'}}> </td>
      <td style={{backgroundColor:'white'}}>#</td>
      <td style={{backgroundColor:'white'}}>REASON</td>
      <td style={{backgroundColor:'white'}}>TAG</td>
      <td style={{backgroundColor:'white'}}>DELETE</td>
      <td style={{backgroundColor:'white'}}> </td>
      <td style={{backgroundColor:'white'}}> </td>
    </tr>
  {item.reports.map((item,idx)=>{return(this.showReport(item,idx))})}
  </>


  )
  }
}
else{
  return(null)
}
  
}
pressItem(idx,mode){
  let users=this.state.users
  if(mode===0) {// on list 
    if(users[idx].pressedList){
      users[idx].pressedList=false
    }
    else{
      users[idx].pressedList=true
    }
  this.setState({users:users})
}
else{
  if(users[idx].pressedActions){
    users[idx].pressedActions=false
  }
  else{
    users[idx].pressedActions=true
  }
  this.setState({users:users})
}

}
async deleteUser(item){
  let resp= await deleteUser(this.props.user.authKey,item.uuID)
  if(resp.success){
   await this.fetchUsers()
 }

}
async writeToUser(){
  let resp=await messageUserUuid(this.props.user.authKey,this.state.chosenUser.uuID,this.state.messageToUser)
  if(resp.success){
    await this.fetchUsers()
  }
  else{
    this.setState({showAlert:true})
  }
  this.setState({messageoverlayvis:false});
}
async gotoUserPage(item){
  await this.props.updateCurrentUser(item)
  await this.props.updateUuid(item.uuID)
this.props.history.push('/userpage')
}
async banEmail(item){
  let resp=await banEmail(this.props.user.authKey,item.email)
  if(resp.success){
    await this.fetchUsers()
  }
}
showListOfActions(item,idx){
  if(item.pressedActions){
  return(
    <tr >
          <td style={{backgroundColor:'white'}}> </td>
          <td><Button variant="danger" style={{display:'inline-block'}} onClick={()=>{this.deleteUser(item)}}>DELETE USER</Button></td>
          <td><Button variant="danger" style={{display:'inline-block'}} onClick={()=>{this.banEmail(item)}}>BAN EMAIL</Button></td>
          <td><Button variant="warning" style={{display:'inline-block'}} onClick={()=>{this.setState({messageoverlayvis:true,chosenUser:item})}}>WRITE MESSAGE</Button></td>
          <td><Button variant="warning" style={{display:'inline-block'}} onClick={()=>{this.gotoUserPage(item)}}>GO TO USER</Button></td>
          
          <td style={{backgroundColor:'white'}}> </td>
          <td style={{backgroundColor:'white'}}> </td>
          <td style={{backgroundColor:'white'}}> </td>

      </tr>
  )
  }
  else{return(null)}
}
renderRow(item,idx){
  let actionsNameList='REPORTS'
  let actionsNameActions='ACTIONS'
  let ispressedList=item.pressedList
  let ispressedActions=item.pressedActions
  if(ispressedList){
    actionsNameList='Hide'
  }
  if(ispressedActions){
    actionsNameActions='Hide'
  }
  return(
    <React.Fragment key={idx}>
      <tr >
          <td>{idx}</td>
          <td>{item.firstName} {item.lastName}</td>
          <td>{item.uuID}</td>
          <td>{item.email}</td>
          <td>{item.friends}</td>
          <td>{item.reports.length}</td>
          <td><Button variant="primary" style={{display:'inline-block'}} onClick={()=>{this.pressItem(idx,0)}}>{actionsNameList}</Button></td>
          <td><Button variant="primary" style={{display:'inline-block'}} onClick={()=>{this.pressItem(idx,1)}}>{actionsNameActions}</Button></td>
      </tr>
      {this.showListOfReports(item,idx)}
      {this.showListOfActions(item,idx)}

      </React.Fragment>

  )
}
renderList(){
  if(this.state.users){
      return(
<Table striped bordered hover >
<thead>
  <tr>
    <th>#</th>
    <th>NAME</th>
    <th>UUID</th>
    <th>EMAIL</th>
    <th>FRIENDS</th>
    <th>REPORTS LEN</th>
    <th>LIST</th>
    <th>ACTIONS</th>
  </tr>
</thead>
<tbody>
  
  {this.state.users.map((item,idx)=>{return(this.renderRow(item,idx))})}
  

</tbody>
</Table>
      )
}
else{return(null)}
}
showOverlay(){
  return(
    <Popup open={this.state.messageoverlayvis} onClose={()=>{ this.setState({messageoverlayvis:false,messageToUser:''}) }} modal>
      {close=>(
        <>
      <div>Write message to user</div>
      <FormControl as="textarea" type="email" placeholder="Write your message here" onInput={(e)=>{this.setState({messageToUser:e.target.value})}}/> 
      
      <Button variant="danger"  style={{display:'inline-block',margin:'1vw'}} onClick={()=>{ this.setState({messageoverlayvis:false});close() }}>CLOSE</Button>
      <Button variant="success"  style={{display:'inline-block',margin:'1vw'}} onClick={()=>{ this.writeToUser() }}>SEND</Button>
  
      </>
      )}
  
    </Popup>
  )
  }
  search(){

let filtered=[]
if(this.state.searchWord!==''){

for(let i=0;i<this.state.usersCopy.length;i++){
  
  if(this.state.usersCopy[i].uuID.includes(this.state.searchWord)){
    filtered.push(this.state.usersCopy[i])
  }
}
}
else{
  filtered=this.state.usersCopy
}
this.setState({users:filtered})
  }
  renderSearchBar(){
      return(
        <div style={{display:'flex',justifyContent:'center'}}>
          <div >
              <div style={{display:'inline-block'}}>
                  <Form.Control type="email" placeholder="User UUID" onInput={(e)=>{this.setState({searchWord:e.target.value})}}/> 
              </div>
              <div style={{display:'inline-block'}}>
                  <Button variant="primary"  onClick={()=>{this.search()}}>Search by uuID</Button>
              </div>
          </div>
        </div>
      )
  }

  showAlert(){
    
    return(
      <Alert show={this.state.showAlert} variant="danger" onClose={() => this.setState({showAlert:false})} dismissible>
      <Alert.Heading>Something went wrong!</Alert.Heading>
      <p>
  We were not able to fetch from server
      </p>
      <hr />
    </Alert>
    )
  }
  async sendToAll(){
    let resp =await sendMessageToAll(this.props.user.authKey,this.state.messageToAll)
    if(resp.success){
      await this.fetchUsers()
    }
  }
  renderWriteMessage(){
    return(
      <div style={{display:'flex',justifyContent:'center',marginTop:'2vh',marginBottom:'5vh'}}>
          <div >
              <Form.Control as='textarea' placeholder="Write message to all" style={{width:'80vw',height:'20vh'}} onInput={(e)=>{this.setState({messageToAll:e.target.value})}}/> 
              <Button variant="success"  onClick={()=>{this.sendToAll()}}>SEND TO ALL</Button>
          </div>
        </div>
    )
  }
  render() {
    return (
        <div>
      <Header route={'Users'}/>
      {this.showAlert()}
      {this.showOverlay()}
      {this.renderWriteMessage()}
      {this.renderSearchBar()}
      {this.renderList()}
           </div> 

      
    );
  }

}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateKey,updateCurrentUser,updateUuid }, dispatch)
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Users)

