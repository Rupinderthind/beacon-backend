import React from 'react'
import Header from '../components/header'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateKey, updateCurrentUser, updateUuid } from '../actions/user'
import {
  getAllUsers,
  deleteReport,
  deleteUser,
  messageUserUuid,
  sendMessageToAll,
  banEmail,
  sendMessageToAllUsers
} from '../func/admin'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import Popup from 'reactjs-popup'
import Alert from 'react-bootstrap/Alert'
import FormControl from 'react-bootstrap/FormControl'

import './styles/users.css'

class Message extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      Login: 'admin',
      Password: 'admin',
      users: null,
      searchWord: '',
      messageoverlayvis: false,
      chosenUser: null,
      messageToUser: '',
      showAlert: false,
      usersCopy: null,
      messageToAll: ''
    }
  }

  async componentDidMount () {
    await this.props.updateCurrentUser(null)
    await this.props.updateUuid(null)
  }
  
  async sendToAll () {
    let resp = await sendMessageToAllUsers(
      this.props.user.authKey,
      this.state.messageToAll
    )
  }
  renderWriteMessage () {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          marginTop: '2vh',
          marginBottom: '5vh'
        }}
      >
        <div>
          <Form.Control
            as='textarea'
            placeholder='Write message to all'
            style={{ width: '80vw', height: '20vh' }}
            onInput={e => {
              this.setState({ messageToAll: e.target.value })
            }}
          />
          <Button
            variant='success'
            onClick={() => {
              this.sendToAll()
            }}
          >
            SEND TO ALL
          </Button>
        </div>
      </div>
    )
  }
  render () {
    return (
      <div>
        <Header route={'Message'} />
        {this.renderWriteMessage()}
      </div>
    )
  }
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { updateKey, updateCurrentUser, updateUuid },
    dispatch
  )
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Message)
