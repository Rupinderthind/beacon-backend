import React from 'react';
import Header from '../components/header'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateKey,updateCurrentUser} from '../actions/user'
import { deleteQuote,getAllUserQuotes} from '../func/admin'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import Popup from "reactjs-popup";



import './styles/users.css';

class UserPage extends React.Component {
  constructor(props) {
    super(props);
    this.state={Login:'admin',Password:'admin',users:null,quotes:null,chosenItem:null,imageOverlayVis:false}
    console.log(this.props.user)
  }

async componentDidMount(){
    await this.fetchUserQuotes()
}
async fetchUserQuotes(){
    let resp=await getAllUserQuotes(this.props.user.authKey,this.props.user.uuID)
    if(resp.success){
        let quotes=resp.quotes
        this.setState({quotes:quotes})
    }
}


async deleteQuote(item){
let resp=await deleteQuote(this.props.user.authKey,item.quoteID)
if(resp.success){
    await this.fetchUserQuotes()
}
}
renderRow(item,idx){


  return(
    <React.Fragment key={idx}>
      <tr >
          <td>{idx}</td>
          <td>{item.title}</td>
          <td>{item.quoteText}</td>
          <td><Button variant="danger" style={{display:'inline-block'}} onClick={()=>{this.deleteQuote(item)}}>DELETE</Button></td>
          <td><Button variant="warning" style={{display:'inline-block'}} onClick={()=>{this.setState({chosenItem:item,imageOverlayVis:true})}}>SHOW IMAGE</Button></td>
      </tr>

      </React.Fragment>

  )
}
renderList(){
  if(this.state.quotes){
      return(
<Table striped bordered hover >
<thead>
  <tr>
    <th>#</th>
    <th>TITLE</th>
    <th>TEXT</th>
    <th>DELETE</th>
    <th>SHOW IMAGE</th>
  </tr>
</thead>
<tbody>
  
  {this.state.quotes.map((item,idx)=>{return(this.renderRow(item,idx))})}
  

</tbody>
</Table>
      )
}
else{return(null)}
}

  showQuoteImage(){
    if(this.state.chosenItem.imageChosen){
    return(
      <>
      <img src={this.state.chosenItem.imageURI} style={{backgroundSize : 'cover',height:'40vh'}} alt='Quote '  />
      <div>{this.state.chosenItem.quoteText}</div>
      </>
    )
    }
    else{
      return(
        <div style={{backgroundColor : this.state.chosenItem.backgroundColor,height:'40vh',textAlign:'center',display:'flex',justifyContent:'center'}}>
          <div style={{alignSelf:'center',fontSize:20}}>{this.state.chosenItem.quoteText}</div>
        </div>
      )
    }
  }
  showImageOverlay(){
    if(this.state.chosenItem){
      console.log(this.state.chosenItem)
    return(
      <Popup open={this.state.imageOverlayVis}  onClose={()=>{ this.setState({imageOverlayVis:false,chosenItem:null}) }} modal>
        {close=>(
        <div style={{alignSelf:'center',display:'flex',flexDirection:'column'}}>
          <div style={{display:'block'}}>{this.state.chosenItem.title}</div>
          {this.showQuoteImage()}
          <Button variant="danger"  style={{display:'inline-block',margin:'1vw'}} onClick={()=>{ this.setState({imageOverlayVis:false,chosenItem:null});close() }}>CLOSE</Button>
          </div>
        )}
    
      </Popup>
    )
        }
        else{return(null)}
  }


renderTitle(){
  return(
    <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',alignSelf:'center',width:'80vw',marginLeft:'10vw'}}>
      <p style={{fontWeight:'bold',fontSize:30}}>Name</p>
      <p  style={{fontSize:25}}>{this.props.user.currentUser.firstName} {this.props.user.currentUser.lastName}</p>
      <p style={{fontWeight:'bold',fontSize:30}}>UUID</p>
      <p style={{fontSize:25}}>{this.props.user.currentUser.uuID}</p>
    </div>
  )
}
  render() {
    return (
        <div>
      <Header route={'Users'}/>
      {this.showImageOverlay()}
      {this.renderTitle()}
      {this.renderList()}
           </div> 

      
    );
  }

}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateKey,updateCurrentUser }, dispatch)
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPage)

