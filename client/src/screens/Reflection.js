import React from 'react';
import Header from '../components/header'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateKey} from '../actions/user'
import { uploadReflection} from '../func/admin'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'
import { SketchPicker } from 'react-color';



import './styles/reflection.css';

class Reflection extends React.Component {
  constructor(props) {
    super(props);
    this.state={showAlertBad:false,showAlertGood:false,chosenImage:null,reflectionMessage:'',textColor:'#fff',imageBase64:null}
    this.imagesize = React.createRef()
    
  }

async componentDidMount(){

}
handleChangeColor = (color) => {
    this.setState({ textColor: color.hex });
  };
pressItem(idx){
    let reports=this.state.reports
    if(reports[idx].pressed){
        reports[idx].pressed=false
    }
    else{
        reports[idx].pressed=true
    }
    this.setState({reports:reports})

}



navtoref(item,tag){
  console.log('reflection')
}



showAlertBad(){
    
  return(
    <Alert show={this.state.showAlertBad} variant="danger" onClose={() => this.setState({showAlertBad:false})} dismissible>
    <Alert.Heading>Something went wrong!</Alert.Heading>
    <p>
We were not able to fetch from server
    </p>
    <hr />
  </Alert>
  )
}
showAlertGood(){
    
  return(
    <Alert show={this.state.showAlertGood} variant="success" onClose={() => this.setState({showAlertGood:false})} dismissible>
    <Alert.Heading>Reflection uploaded!</Alert.Heading>
    <p>
    We successfuly uploaded reflection
    </p>
    <hr />
  </Alert>
  )
}



handleChange(item){
    
}
showInputImage(){

    return(
        <div>
            <input type="file" onChange={(event)=>{this.setState({chosenImage:event.target.files[0]})} }/>
        </div>
    )
}
showImageText(){
    if(this.state.chosenImage ){
        return(

            <div className="image-wrapper">
                    <img src={ URL.createObjectURL(this.state.chosenImage) } style={{backgroundSize : 'cover',width:'40vw',height:'40vh'}} alt='Reflection ' ref={this.imagesize} />
                    <div style={{textAlign: 'center',color:this.state.textColor,width:'40vw',marginTop:'-40%',wordWrap:'break-word',alignItems:'center'}}>{this.state.reflectionMessage}</div>
                    
            </div>
            

        )
    }
    else{
        return(null)
    }
}
 getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

async post(){
  if(this.state.chosenImage){
    
    let imgbase64= await this.getBase64(this.state.chosenImage)
    if(imgbase64){
      let resp=await uploadReflection(this.props.user.authKey,this.state.reflectionMessage,this.state.textColor,imgbase64)
      if(resp.success){
        this.setState({showAlertGood:true})
      }
      else{
        this.setState({showAlertBad:true})
      }
    }
  }
}
showTextInput(){
    return(
        < >

                <FormControl as="textarea" type="email" placeholder="Write reflection text here" onInput={(e)=>{this.setState({reflectionMessage:e.target.value})}}/> 


                <Button variant="success"  style={{marginTop:'2vh'}} onClick={()=>{ this.post() }}>UPLOAD REFLECTION</Button>

                <div style={{justifyContent:'center',display:'flex',marginTop:'2vh',marginBottom:'2vh'}}>
                <SketchPicker color={ this.state.textColor } onChangeComplete={ this.handleChangeColor }/> 
                </div>
                

        </>
    )
}
  render() {
    return (
        <div>
      <Header route={'Reflection'}/>
      
        <div>{this.showAlertBad()}</div>
        <div>{this.showAlertGood()}</div>
        <div>{this.showInputImage()}</div>
        <div>{this.showImageText()}</div>
        <div style={{position:'absolute',top:'50vh',left:'25vw',width:'50vw'}}>{this.showTextInput()}</div>
        
      </div> 

      
    );
  }

}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateKey }, dispatch)
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reflection)

