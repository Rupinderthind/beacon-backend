import React from 'react';
import Header from '../components/header'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateKey,updateCurrentUser,updateUuid} from '../actions/user'
import { banEmail,getBanned} from '../func/admin'

import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'


import './styles/users.css';

class Bans extends React.Component {
  constructor(props) {
    super(props);
    this.state={banned:null}
    
  }

async componentDidMount(){
    await this.fetchBanned()
    await this.props.updateCurrentUser(null)
  await this.props.updateUuid(null)
}
async fetchBanned(){
    let resp=await getBanned(this.props.user.authKey)
    if(resp.success){
        let vals=resp.banned
        this.setState({banned:vals})
    }
}


async unban(item){
    let resp=await banEmail(this.props.user.authKey,item[0].email)
    if(resp.success){
      await this.fetchBanned()
    }
}


renderRow(item,idx){
  return(
    <React.Fragment key={idx}>
      <tr >
          <td>{idx}</td>
          <td>{item[0].firstName} {item[0].lastName}</td>
          <td>{item[0].uuID}</td>
          <td>{item[0].email}</td>
          <td><Button variant="success" style={{display:'inline-block'}} onClick={()=>{this.unban(item)}}>UNBAN</Button></td>
      </tr>

      </React.Fragment>

  )
}
renderList(){
  if(this.state.banned){
      return(
<Table striped bordered hover >
<thead>
  <tr>
    <th>#</th>
    <th>NAME</th>
    <th>UUID</th>
    <th>EMAIL</th>
    <th>UNBAN</th>
  </tr>
</thead>
<tbody>
  
  {this.state.banned.map((item,idx)=>{return(this.renderRow(item,idx))})}
  

</tbody>
</Table>
      )
}
else{return(null)}
}



  render() {
    return (
        <div>
      <Header route={'Bans'}/>
      {this.renderList()}
           </div> 

      
    );
  }

}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateKey,updateCurrentUser,updateUuid }, dispatch)
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Bans)

