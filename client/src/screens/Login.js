import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import './styles/loginformstyle.css';
import { updateKey} from '../actions/user'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { login } from '../func/admin'
import Alert from 'react-bootstrap/Alert'
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state={login:'',password:'',showAlert:false,passwordCovered:''}
  }

  async componentDidMount(){
    await this.props.updateKey('')

  }
  async handleLogin(){

      let resp=await login(this.state.login,this.state.password)
      if(resp.success){
        await this.props.updateKey(resp.authKey)
        this.setState({showAlert:false})
        this.props.history.push('/main')
      }
      else{
        this.setState({showAlert:true})
      }

  }
  showAlert(){
    
    return(
      <Alert show={this.state.showAlert} variant="danger">
      <Alert.Heading>Login failed!</Alert.Heading>
      <p>
Wrong login or password!
      </p>
      <hr />

    </Alert>
    )
  }


  
  loginForm(){
    return(



      <div className="loginForm">
        <div style={{justifyContent:'center'}}>
          <TextField  value={this.state.login} label="Login" onInput={(e)=>{this.setState({login:e.target.value,showAlert:false}) }} />
        </div>
        <div>
          <TextField value={this.state.password} type='password' style={{marginRight:'2vw',marginLeft:'2vw'}} label="Password" onInput={(e)=>{this.setState({password:e.target.value}) }} />
        </div>
        <Button onClick={()=>{this.handleLogin()}}>LOGIN</Button>
      </div>


    )
  }


  render() {
    return (
      <div>
        {this.showAlert()}
        {this.loginForm() }          
        </div>
      
    );
  }

}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateKey }, dispatch)
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)

