import React from 'react';
import Header from '../components/header'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateKey} from '../actions/user'
import { getAllReports,deleteReport,deleteQuote,deleteUser,messageUser} from '../func/admin'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import Alert from 'react-bootstrap/Alert'
import Popup from "reactjs-popup";



import './styles/reports.css';

class Reports extends React.Component {
  constructor(props) {
    super(props);
    this.state={Login:'admin',Password:'admin',reports:null,searchWord:'',showAlert:false,messageoverlayvis:false,messageToUser:'',chosenItem:'',chosenTag:'',imageOverlayVis:false,chosenItemImage:null}
  }

async componentDidMount(){
    await this.fetchReports()
}
async fetchReports(){
    let resp=await getAllReports(this.props.user.authKey)
    if(resp.success){
        let obj=0
        let newreports=[]
        for(let i=0;i<resp.reports.length;i++){
            obj={...resp.reports[i],pressed:false}
            newreports.push(obj)
        }
        this.setState({reports:newreports,showAlert:false})
    }
}
pressItem(idx){
    let reports=this.state.reports
    if(reports[idx].pressed){
        reports[idx].pressed=false
    }
    else{
        reports[idx].pressed=true
    }
    this.setState({reports:reports})

}
renderRow(item,idx){
    let actionsName='Actions'
    let ispressed=item.pressed
    if(ispressed){
        actionsName='Hide'
    }
    let tag=''
    switch(item.tag){
      case 'quote':tag='QUOTE'
                    break;
      case 'user':tag='USER'
                    break;
      case 'reflection':tag='REFLECTION'
                    break;
      case 'deleteaccount':tag='DELETE ACCOUNT'
                    break;
      default: tag='none'
                break;
    }
    return(
      <React.Fragment key={idx}>
        <tr >
            <td>{idx}</td>
            <td>{tag}</td>
            <td>{item.reason}</td>
            <td><Button variant="primary" style={{display:'inline-block'}} onClick={()=>{this.pressItem(idx)}}>{actionsName}</Button></td>
            <td style={{backgroundColor:'white'}}>
                {tag==='QUOTE'?
                <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
                  <Button variant="warning" style={{display:'inline-block'}} onClick={()=>{this.setState({imageOverlayVis:true,chosenItemImage:item})}}>SHOW QUOTE</Button>
                </div>
                
                : <> </>}
                </td>
        </tr>
        {this.renderActionDropdown(item,idx,tag)}

        </React.Fragment>

    )
}
async deleteReport(item){

    let resp=await deleteReport(this.props.user.authKey,item.id)
    if(resp.success){
        await this.fetchReports()
    }
}
async deleteTag(item,tag){
  let action=-1
  let resp=0
  switch(tag){
    case 'QUOTE':
           resp= await deleteQuote(this.props.user.authKey,item._doc.quoteID)
           if(resp.success){
            await this.fetchReports()
          }
          else{
            this.setState({showAlert:true})
          }
          break;
    case 'USER':action=1
          console.log(item)
          resp= await deleteUser(this.props.user.authKey,item._doc.uuID)
           if(resp.success){
            await this.fetchReports()
          }
          else{
            this.setState({showAlert:true})
          }
          break;
    case 'REFLECTION':action=2
          break;
    default:action=-1
            break;
  }

  console.log(action)
  console.log(item)
}
async writeToUser(){
  let resp=await messageUser(this.props.user.authKey,this.state.chosenItem.id,this.state.messageToUser)
  if(resp.success){
    await this.fetchReports()
  }
  else{
    this.setState({showAlert:true})
  }
  this.setState({messageoverlayvis:false});
}


navtoref(item,tag){
  console.log('reflection')
}

renderActionDropdown(item,idx,tag){
    
    if(item.pressed){
      if(tag!=='DELETE ACCOUNT' &&tag!=='REFLECTION'){
        console.log(tag)
        return(

            <tr style={{backgroundColor:'#0c56eb'}}>
                <td>-</td>
                <td>
                  <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
                    <Button variant="secondary" style={{display:'inline-block',verticalAlign:'center'}} onClick={()=>{this.deleteReport(item)}}>DELETE REPORT</Button>
                  </div>
                </td>
                <td >
                  <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
                    <Button variant="danger"  style={{display:'inline-block'}} onClick={()=>{this.deleteTag(item,tag)}}>DELETE {tag}</Button>
                  </div>
                </td>
                <td>
                  <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
                    <Button variant="warning" style={{display:'inline-block'}} onClick={()=>{this.setState({messageoverlayvis:true,chosenItem:item,chosenTag:tag})}}>WRITE TO USER</Button>
                  </div>
                </td>

            </tr>
           
        )
      }
      else{
        if(tag==='DELETE ACCOUNT'){
        return(
          <tr style={{backgroundColor:'#0c56eb'}}>
                <td>-</td>
                <td>
                  <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
                    <Button variant="secondary" style={{display:'inline-block',verticalAlign:'center'}} onClick={()=>{this.deleteReport(item)}}>DELETE REPORT</Button>
                  </div>
                </td>
                <td style={{backgroundColor:'white'}}>-</td>
                <td style={{backgroundColor:'white'}}>-</td>
                <td style={{backgroundColor:'white'}}>-</td>
            </tr>
        )
        }
        else{
          return(
            <tr style={{backgroundColor:'#0c56eb'}}>
                <td style={{backgroundColor:'white'}}>-</td>
                <td>
                  <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
                    <Button variant="secondary" style={{display:'inline-block',verticalAlign:'center'}} onClick={()=>{this.deleteReport(item)}}>DELETE REPORT</Button>
                  </div>
                </td>
                <td >
                  <div style={{verticalAlign:'middle',display:'inline-block',margin:'1vh'}}>
                    <Button variant="danger"  style={{display:'inline-block'}} onClick={()=>{this.navtoref(item,tag)}}>REFLECTION PAGE</Button>
                  </div>
                </td>
                <td style={{backgroundColor:'white'}}>-</td>
                <td style={{backgroundColor:'white'}}>-</td>
            </tr>
          )
        }
      }
    }
    else{return(null)}
}
showOverlay(){
return(
  <Popup open={this.state.messageoverlayvis} onClose={()=>{ this.setState({messageoverlayvis:false,messageToUser:''}) }} modal>
    {close=>(
      <>
    <div>Write message to user</div>
    <FormControl as="textarea" type="email" placeholder="Write your message here" onInput={(e)=>{this.setState({messageToUser:e.target.value})}}/> 
    
    <Button variant="danger"  style={{display:'inline-block',margin:'1vw'}} onClick={()=>{ this.setState({messageoverlayvis:false});close() }}>CLOSE</Button>
    <Button variant="success"  style={{display:'inline-block',margin:'1vw'}} onClick={()=>{ this.writeToUser() }}>SEND</Button>

    </>
    )}

  </Popup>
)
}
showQuoteImage(){
  if(this.state.chosenItemImage._doc.imageChosen){
  return(
    <>
    <img src={this.state.chosenItemImage._doc.imageURI} style={{backgroundSize : 'cover',height:'40vh'}} alt='Quote '  />
    <div>{this.state.chosenItemImage._doc.quoteText}</div>
    </>
  )
  }
  else{
    return(
      <div style={{backgroundColor : this.state.chosenItemImage._doc.backgroundColor,height:'40vh',textAlign:'center',display:'flex',justifyContent:'center'}}>
        <div style={{alignSelf:'center',fontSize:20}}>{this.state.chosenItemImage._doc.quoteText}</div>
      </div>
    )
  }
}
showImageOverlay(){
  if(this.state.chosenItemImage){
    console.log(this.state.chosenItemImage._doc)
  return(
    <Popup open={this.state.imageOverlayVis}  onClose={()=>{ this.setState({imageOverlayVis:false,chosenItemImage:null}) }} modal>
      {close=>(
      <div style={{alignSelf:'center',display:'flex',flexDirection:'column'}}>
        <div style={{display:'block'}}>{this.state.chosenItemImage._doc.title}</div>
        {this.showQuoteImage()}
        <Button variant="danger"  style={{display:'inline-block',margin:'1vw'}} onClick={()=>{ this.setState({imageOverlayVis:false});close() }}>CLOSE</Button>
        </div>
      )}
  
    </Popup>
  )
      }
      else{return(null)}
}
showAlert(){
    
  return(
    <Alert show={this.state.showAlert} variant="danger" onClose={() => this.setState({showAlert:false})} dismissible>
    <Alert.Heading>Something went wrong!</Alert.Heading>
    <p>
We were not able to fetch from server
    </p>
    <hr />
  </Alert>
  )
}

  renderList(){
    if(this.state.reports){
        return(
<Table striped bordered hover >
  <thead>
    <tr>
      <th>#</th>
      <th>TAG</th>
      <th>REASON</th>
      <th>ACTION</th>
      <th>SHOW</th>
    </tr>
  </thead>
  <tbody>
    
    {this.state.reports.map((item,idx)=>{return(this.renderRow(item,idx))})}
    

  </tbody>
</Table>
        )
}
else{return(null)}
}




  render() {
    return (
        <div>
      <Header route={'Reports'}/>
      {this.showOverlay()}
      {this.showImageOverlay()}
      {this.showAlert()}
      {this.renderList()}
           </div> 

      
    );
  }

}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateKey }, dispatch)
}

const mapStateToProps = state => {
  return {
      user: state.user
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reports)

