import React from 'react';
import './headerstyle.css';
import {Link} from 'react-router-dom';
export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state={menuopen:false,activeItem:'',availableNames:[{name:'Home',navigate:'/main'},{name:'Users',navigate:'/users'}, 
    {name:'Reports',navigate:'/reports'}, {name:'Reflection',navigate:'/reflection'}, {name:'Support',navigate:'/contactedsupport'}, {name:'Bans',navigate:'/bans'},{name:'Message',navigate:'/message'}],hoveringOn:-1,colorBuffer:0}
  }
  handleItemClick(index){
    
    this.setState({activeItem:index},()=> console.log(this.state.availableNames[this.state.activeItem]))
    
  }




linkuj(){
  return
}
renderNames(){
  return(
    this.state.availableNames.map((item,index)=>{
      return(
      <div className="headerInner"  key={index}> 
          <Link to={item.navigate}  style={{ textDecoration: 'none',color: this.props.route===item.name?'black':' white',marginLeft:'2vw',marginRight:'2vw' }}>
            {item.name}
          </Link> 
        </div>
      )
    })
  )
}
  render() {
    return (
        
      <div className="headerOuter">
        {this.renderNames()}
    </div>
      
    );
  }

}
