import React, { Component } from 'react';
import './App.css';

import {
  Route,
  Switch,withRouter
} from 'react-router-dom';


import Main from './screens/Main';
import Login from './screens/Login';
import Users from './screens/Users';
import Reports from './screens/Reports';
import Reflection from './screens/Reflection';
import Support from './screens/Support';
import UserPage from './screens/UserPage';
import Bans from './screens/Bans';
import Message from './screens/Message';











class App extends Component {
  render() {
    return (
      <div className="App">

        
        <div className="App-intro">
          <Switch>
            <Route path="/" exact component={withRouter(Login) } />
            <Route path="/main" exact component={withRouter(Main) } />
            <Route path="/users" exact component={withRouter(Users) } />
            <Route path="/reports" exact component={withRouter(Reports) } />
            <Route path="/reflection" exact component={withRouter(Reflection) } />
            <Route path="/contactedsupport" exact component={withRouter(Support) } />
            <Route path="/userpage" exact component={withRouter(UserPage) } />
            <Route path="/bans" exact component={withRouter(Bans) } />
            <Route path='/message' exact component={withRouter(Message)} />
            
            
            
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
