import { combineReducers } from 'redux'
import { UPDATE_KEY,UPDATE_CURRENT_USER,UPDATE_UUID} from '../actions/user'

const user = (state = {}, action) => {
    switch (action.type) {
        case UPDATE_KEY:
            return { ...state, authKey: action.payload }
        case UPDATE_CURRENT_USER:
            return { ...state, currentUser: action.payload }
        case UPDATE_UUID:
            return { ...state, uuID: action.payload }

            
                
        default:
            return state
    }
}

const rootReducer = combineReducers({
    user
})

export default rootReducer
