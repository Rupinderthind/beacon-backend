import axios from 'axios';
let myurl='https://beacon-backend.herokuapp.com/' 
export async function login(login,password){
    let url=myurl+'admin/login'
    let payload = {
        login: login,
        password: password
      };
    let resp=await axios.post(url,  payload )
    let dat=await resp.data
    return dat
}
export async function getAllUsers(authKey){
  let url=myurl+'admin/getAllUsers'
  let payload = {
    authKey: authKey,
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function getAllReports(authKey){
  let url=myurl+'admin/getAllReports'
  let payload = {
    authKey: authKey,
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function deleteReport(authKey,reportID){
  let url=myurl+'admin/deleteReport'
  let payload = {
    authKey: authKey,
    reportID: reportID
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function deleteQuote(authKey,quoteID){
  let url=myurl+'admin/deleteQuote'
  let payload = {
    authKey: authKey,
    quoteID: quoteID
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function deleteUser(authKey,userID){
  let url=myurl+'admin/deleteUser'
  let payload = {
    authKey: authKey,
    userID: userID
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function messageUser(authKey,reportID,messageContent){
  let url=myurl+'admin/messageUser'
  let payload = {
    authKey: authKey,
    reportID: reportID,
    messageContent:messageContent
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function messageUserUuid(authKey,uuID,messageContent){
  let url=myurl+'admin/messageUserUuid'
  let payload = {
    authKey: authKey,
    uuID: uuID,
    messageContent:messageContent
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}

export async function uploadReflection(authKey,reflectionText,fontColor,base64Img){
  let url=myurl+'admin/uploadReflection'
  let payload = {
    authKey: authKey,
    reflectionText: reflectionText,
    fontColor:fontColor,
    base64Img:base64Img
    };
  let resp=await axios.post(url,  payload )
  console.log(resp)
  let dat=await resp.data
  return dat
}
export async function getSupportContacts(authKey){
  let url=myurl+'admin/getSupportContacts'
  let payload = {
    authKey: authKey,
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function deleteContact(authKey,id){
  let url=myurl+'admin/deleteContact'
  let payload = {
    authKey: authKey,
    contactID:id
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function respondToContact(authKey,email,message,id){
  let url=myurl+'admin/respondToContact'
  let payload = {
    authKey: authKey,
    userEmail:email,
    messageToUser:message,
    contactID:id
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function getAllUserQuotes(authKey,uuID){
  let url=myurl+'admin/getAllUserQuotes'
  let payload = {
    authKey: authKey,
    uuID:uuID
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function sendMessageToAll(authKey,message){
  let url=myurl+'admin/sendMessageToAll'
  let payload = {
    authKey: authKey,
    message:message
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function sendMessageToAllUsers(authKey,message){
  let url=myurl+'admin/sendMessageToAllUsers'
  let payload = {
    authKey: authKey,
    message:message
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function banEmail(authKey,email){
  let url=myurl+'admin/banEmail'
  let payload = {
    authKey: authKey,
    email:email
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}
export async function getBanned(authKey){
  let url=myurl+'admin/getBanned'
  let payload = {
    authKey: authKey
    };
  let resp=await axios.post(url,  payload )
  let dat=await resp.data
  return dat
}





