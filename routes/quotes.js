const tools = require('../src/emai');
const express = require('express')
const router = express.Router()
const crypto = require("crypto");
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SGAPI_KEY);
const User = require('../models/User')
const Quote = require('../models/Quote')
const AWS = require('aws-sdk');
require('dotenv').config()
AWS.config.update({region: 'us-west-2'});
console.log(process.env.AWS_ACCESKEYID,"fdgfgdfgdfgfdgdfgAWS_ACCESKEYID")
const s3 = new AWS.S3({accessKeyId:process.env.AWS_ACCESKEYID ,secretAccessKey: process.env.AWS_SECRETKEY,params:{Bucket:process.env.AWS_BUCKETNAME}});
var gcm = require('node-gcm');
var sender = new gcm.Sender(process.env.FCM_SERVER_KEY);


// Getting all 
router.post('/', async (req, res) => {///needs testing
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
        let quotes=await Quote.find()
      
      res.status(200).json({success:true,quotes:quotes})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getNumUserQuotes', async (req, res) => {///needs testing
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})

    if(auth_user){
        let filteredQuotes=await Quote.find({$and: [ {addedByUuid: req.body.uuID},{isQuote: {$exists: true, $ne: true}} ]},{dateAdded:1,quoteID:1,title:1,visibleBy:1,userFriendList:1,quoteText:1})
        console.log(filteredQuotes,"filteredQuotes")
        let userwhoposted=await User.findOne({ 'uuID': req.body.uuID},{friends:1})
        let newObj=0
        let filteredadded=[]
        for(let i=0;i<filteredQuotes.length;i++){

          newObj={dateAdded:filteredQuotes[i].dateAdded,quoteID:filteredQuotes[i].quoteID,title:filteredQuotes[i].title,visible:true}

          if(filteredQuotes[i].visibleBy==1){ //friends
            newObj.visible=false
            for(let p=0;p<userwhoposted.friends.length;p++){
              if(userwhoposted.friends[p].uuid==auth_user.uuID && userwhoposted.friends[p].accepted ){
                newObj.visible=true
              }
            }
            if(req.body.uuID==auth_user.uuID){
              newObj.visible=true
            }
            
          }
          if(filteredQuotes[i].visibleBy==2){ //me only
            if(req.body.uuID!=auth_user.uuID){
              newObj.visible=false
            }
          }
          filteredadded.push(newObj)
        }
        let quotetext=0;
        let repeats=0;
        let countedQuotes=0
        for(let i=0;i<filteredQuotes.length;i++){ //searching for "" 
        quotetext=filteredQuotes[i].quoteText
        repeats=0
          for(let z=0;z<quotetext.length;z++){
                if(quotetext[z]=='"'){
                  repeats++;
                }
          }
          if(repeats!=0 && repeats%2==0){
            countedQuotes++;
          }

        }
        
        res.status(200).json({success:true,quotes:filteredadded,buildsLength:filteredQuotes.length,quotesLength:countedQuotes})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

router.post('/getNumUserQuotes', async (req, res) => {///needs testing
  try {
    let auth_user= await User.find({ 'authKey': req.body.authKey})
  }catch(err) {

  }
})

router.post('/getrecentlySearched', async (req, res) => {///needs testing
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
      
        res.status(200).json({success:true,recentlySearched:auth_user.recentlySearched})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getAllPosts', async (req, res) => {///needs testing
  try {
    var start = new Date(req.body.date);
    start.setHours(0,0,0,0);

    var end = new Date(req.body.date);
    end.setHours(23,59,59,999);
    console.log(start,end,"startstart")
    let posts= await Quote.find({$and: [{ dateAdded: { $gt: start } }, { dateAdded: { $lte: end } }, {addedByUuid: req.body.uuID},{isQuote: {$exists: true, $ne: true}} ]}).sort({dateAdded:-1})
    console.log(posts,"posts")
    res.status(200).json({success:true,posts})
  } catch (err) {
    console.log(err,"errrrr")
    res.status(500).json({ message: err.message })
  }
})
router.post('/deleterecentlySearched', async (req, res) => {///needs testing
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
        let deleteWord=req.body.deleteWord
        for(let i=0;i<auth_user.recentlySearched.length;i++){
          if(auth_user.recentlySearched[i]==deleteWord){
            auth_user.recentlySearched.splice(i,1)
          }
        }
        await auth_user.save()
        res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getOne', async (req, res) => {///needs testing
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
        let quote=await Quote.find({'quoteID':req.body.quoteID})
      res.status(200).json({success:true,quote:quote})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({success:false, message: err.message })
  }
})
router.post('/getRandomQuote', async (req, res) => {
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
        let expression='.*'+req.body.searchWord+'.*'

        let quote=await Quote.findOne({'quoteText': {'$regex' : expression, '$options' : 'i'},'$or':[{'visibleBy':0},
        {'visibleBy':2,'addedByUuid':auth_user.uuID},{'visibleBy':1,"userFriendList":{"$elemMatch":{"uuid":auth_user.uuID}} }] },
        {likedBy:1,style:1,quoteText:1,quoteID:1,likes:1,addedByUuid:1}).sort({dateAdded:-1}).skip(req.body.offset)
        console.log(quote,"quotequote")
        if(quote){
            let fetcheduser=await User.findOne({ 'uuID': quote.addedByUuid})
            let dowehaveinfavorites=false
            if(auth_user.favoriteQuotes.includes(quote.quoteID)){
              dowehaveinfavorites=true
            }

            let myobj={firstName:fetcheduser.firstName,lastName:fetcheduser.lastName,displayTitle:fetcheduser.displayTitle,avatarURI:fetcheduser.avatarURI,uuID:quote.addedByUuid}
            res.status(200).json({success:true,quote:quote,currentUser:myobj,favorite:dowehaveinfavorites})

        }
        else{
          res.status(300).json({success:false})

        }
        
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ success:false,message: err.message })
  }
})

router.post('/getUserQuotes', async (req, res) => {///needs testing
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
        let quotes=await Quote.find({'addedByUuid':req.body.addedByUuid})
      
      res.status(200).json({success:true,quotes:quotes} )
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/delete', async (req, res) => {
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
        
      //we need to remove from friends notifications and my 
      //first remove this quote from my notifications
      for(let i=0;i<auth_user.notifications.length;i++){
        if('quoteID' in auth_user.notifications[i]){
          if(auth_user.notifications[i].quoteID==req.body.quoteID){
            auth_user.notifications.splice(i,1)
          }
        }
      }
      await auth_user.save()
      
      let foundfriend=0
      for(let p=0;p<auth_user.friends.length;p++){//scroll through all of user friends
        foundfriend=await User.findOne({ 'uuID': auth_user.friends[p].uuID},{notifications:1}) 
        if(foundfriend){//check if available
          for(let z=0;z<foundfriend.notifications.length;z++){
            if('quoteID' in foundfriend.notifications[z]){
              if(foundfriend.notifications[z].quoteID==req.body.quoteID){
                foundfriend.notifications.splice(z,1)
              }
            }
          }
          await foundfriend.save() 
        }
        
      
      }
      await Quote.deleteOne({'quoteID':req.body.quoteID,'addedByUuid':auth_user.uuID})
      
      res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/add', async (req, res) => { 
    try {
      console.log(process.env.AWS_ACCESKEYID)
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){

        let chosenquoteid=crypto.randomBytes(20).toString('hex')
        let testedquote=await Quote.findOne({'quoteID':chosenquoteid})


        
        while(testedquote){
          chosenquoteid=crypto.randomBytes(20).toString('hex')
          testedquote=await Quote.findOne({'quoteID':chosenquoteid})
        }

        let filteredUserFriendsAndMe=[]
        let obj=0
        for(let j=0;j<auth_user.friends.length;j++){
          obj={uuid:auth_user.friends[j].uuID}
          filteredUserFriendsAndMe.push(obj)
        }
        obj={uuid:auth_user.uuID}
        filteredUserFriendsAndMe.push(obj)



        let action = 6
        let isQuote = false
        if (req.body.quoteText.charAt(0) === '“' || req.body.quoteText.charAt(0) === '‘' || req.body.quoteText.slice(-1) === '‘' || req.body.quoteText.slice(-1) === '“') {
          console.log('line 331')
          action = 1
          isQuote = true
        }

        let quote = new Quote({
            addedByFirstName: auth_user.firstName,
            addedByLastName: auth_user.lastName,
            addedByUuid:auth_user.uuID,
            title: req.body.title,
            style:req.body.style,
            backgroundColor:req.body.backgroundColor,
            quoteText:req.body.quoteText,
            comments:[],
            likedBy:[],
            dateAdded:Date.now(),
            likes:0,
            imageURI:"",
            imageChosen:req.body.imageChosen,
            visibleBy:req.body.shareWith,
            userFriendList:filteredUserFriendsAndMe,
            quoteID: chosenquoteid,
            isQuote: isQuote
        })
        console.log(quote,"quote added")
          //0 everyone,1 friends,2 me only
        if (req.body.shareWith != 2) { //notify always,only if not only for me
            let userName=auth_user.firstName+' '+auth_user.lastName
          for (let i = 0; i < auth_user.friends.length; i++){
                let uuID= auth_user.friends[i].uuID
                
                let notification={"action": action, "name": userName,'title':req.body.title,'uuID':auth_user.uuID,
                'notificationID':crypto.randomBytes(20).toString('hex'),'quoteID':quote.quoteID,'addedAt':Date.now(),'avatarURI':auth_user.avatarURI} //action -1 quote added 
                await User.updateOne({ 'uuID': uuID }, { $push: { notifications: notification } });
                let addedUser=await User.findOne({ 'uuID': uuID });
                var message = new gcm.Message({
                  priority: 'high',
                  contentAvailable: true,
                  data: {
                    key1: quote.quoteID,
                  },
                  notification: {
                    title: "Transfigure",
                    icon: "./playstore.png",
                    body: `${userName} added a new quote`
                  }
                });
                console.log(addedUser.deviceHash,"auth_user.friends[i].deviceHash")
                sender.send(message, { registrationTokens: [addedUser.deviceHash] }, function (err, response) {
                  if (err) console.error(err,"sdsdasdsdsdasdsd");
                  else console.log(response,"responsenotification");
                });
            }
          }

          
          
          if(req.body.imageChosen){
              if(req.body.imagetoSend.length){
                let imageurl=[]
                req.body.imagetoSend.map(async item => {
                  let mykey=chosenquoteid+Date.now()+'_image'
                  imageurl.push(await uploadImages(item,mykey))
                  Promise.all(imageurl).then(async url => {
                    quote.imageURI=url
                    await quote.save()
                    res.status(200).json({success:true})
                  })
                  // s3.putObject(data, async function(err, data){
                  //     if (err) { 
                  //       console.log(err);
                  //       console.log('Error uploading data: ', data);
                  //       res.status(300).json({success:false}) 
                  //     } else {
                  //       console.log(data)
                  //       console.log(`succesfully uploaded the image at ${imageurl}`);
                  //       imageurl.push(`https://${process.env.AWS_BUCKETNAME}.s3.eu-west-2.amazonaws.com/${mykey}`)
                  //     }
                  // });
                })
          }
          // else{
          //       quote.imageURI=req.body.imagetoSend
          //       await quote.save()
          //       res.status(200).json({success:true})
          // }
          }
          else {
            await quote.save()
            res.status(200).json({success:true})
          }


          
      }
      else {
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/addComment', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){

        let comID=crypto.randomBytes(20).toString('hex')



        let userName=auth_user.firstName+' '+auth_user.lastName
        let comment={
          commentText:req.body.commentText,
          commentID:comID,
          replies:[],
          addedBy:userName,
          addedByUuid:auth_user.uuID,
          addedAt:Date.now(),
          likes:0,
          repliesNumber:0,
          avatarURI:auth_user.avatarURI,
          likedBy:[]
        };
        let commentedQuote=await Quote.findOne({ 'quoteID': req.body.quoteID });
        commentedQuote.comments.push(comment);
        
        //await Quote.updateOne({ 'quoteID': req.body.quoteID }, { $push: { comments: comment } });

        let notification={"action": 2, "name": userName,'text':req.body.commentText,'uuID':auth_user.uuID,
        'notificationID':crypto.randomBytes(20).toString('hex'),'commentID':comID,
        'quoteID':req.body.quoteID,'addedAt':comment.addedAt,'avatarURI':auth_user.avatarURI} //action -2 commented qoute
        await User.updateOne({ 'uuID': commentedQuote.addedByUuid }, { $push: { notifications: notification } });
        await commentedQuote.save()
        let addedUser=await User.findOne({ 'uuID': commentedQuote.addedByUuid });
        var message = new gcm.Message({
          priority: 'high',
          contentAvailable: true,
          data: {
            key1: req.body.quoteID,
          },
          notification: {
            title: "Transfigure",
            icon: "./playstore.png",
            body: `${userName} commented on your quote`
          }
        });
        sender.send(message, { registrationTokens: [addedUser.deviceHash] }, function (err, response) {
          if (err) console.error(err,"sdsdasdsdsdasdsd");
          else console.log(response,"responsenotification");
        });
        res.status(200).json({success:true})
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })

  router.post('/deleteCommentOfQuote', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){

        let foundQuote=await Quote.findOne({ 'quoteID': req.body.quoteID})
        let commentsMy=foundQuote.comments
        for(let i=0;i<commentsMy.length;i++){
          if(commentsMy[i].commentID==req.body.commentID){
            commentsMy.splice(i,1)
          }
        }
        await Quote.updateOne({ 'quoteID': req.body.quoteID }, { '$set': { comments: commentsMy } })

        //comment is deleted ,now we need to delete notification from users notificaton
        let thatUser=await User.findOne({ 'uuID': foundQuote.addedByUuid})
        let usersnotifi=thatUser.notifications
        for(let p=0;p<usersnotifi.length;p++){
          if(usersnotifi[p].action==2 && usersnotifi[p].commentID==req.body.commentID){
            usersnotifi.splice(p,1)
          }
        }
        await User.updateOne({ 'uuID': foundQuote.addedByUuid }, { '$set': { notifications: usersnotifi } })
        res.status(200).json({success:true})
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })

  router.post('/deleteCommentOfComment', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){

        let foundQuote=await Quote.findOne({ 'quoteID': req.body.quoteID})
        let commentsMy=foundQuote.comments
        for(let i=0;i<commentsMy.length;i++){
          if(commentsMy[i].commentID==req.body.commentID){
              for(let z=0;z<commentsMy[i].replies.length;z++){
                if(commentsMy[i].replies[z].replyID==req.body.replyID){
                  commentsMy[i].replies.splice(z,1)
                  commentsMy[i].repliesNumber-=1
                  console.log('found')
                }
              }
          }
        }
        await Quote.updateOne({ 'quoteID': req.body.quoteID }, { '$set': { comments: commentsMy } })
        //comment is deleted ,now we need to delete notification from users notificaton
        let thatUser=await User.findOne({ 'uuID': req.body.addedByUuid})
        let usersnotifi=thatUser.notifications
        for(let p=0;p<usersnotifi.length;p++){
          if(usersnotifi[p].action==3 && usersnotifi[p].replyID==req.body.replyID){
            usersnotifi.splice(p,1)
          }
        }
        await User.updateOne({ 'uuID': req.body.addedByUuid }, { '$set': { notifications: usersnotifi } })
        res.status(200).json({success:true})
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
   
  router.post('/replytocomment', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){

        let replyID=crypto.randomBytes(20).toString('hex')
        let userName=auth_user.firstName+' '+auth_user.lastName
        let reply={
          replyText:req.body.replyText,
          replyID:replyID,
          replies:[],
          addedBy:userName,
          addedAt:Date.now(),
          addedByUuid:auth_user.uuID,
          likes:0,
          repliesNumber:0,
          avatarURI:auth_user.avatarURI
        };
        await Quote.updateOne({"quoteID":req.body.quoteID,"comments":{"$elemMatch":{"commentID":req.body.commentID}}}
        ,{"$push":{"comments.$.replies":reply},"$inc":{"comments.$.repliesNumber":1}}); 
        let commentedQuote=await Quote.findOne({ 'quoteID': req.body.quoteID });
        //now we need to find our comment
        let whoPostedComment='';
        for (let i=0;i<commentedQuote.comments.length;i++){
          if(commentedQuote.comments[i].commentID==req.body.commentID){
            whoPostedComment=commentedQuote.comments[i].addedByUuid
          }
        }
        
        


        let notification={"action": 3, "name": userName,'text':req.body.replyText,
        'notificationID':crypto.randomBytes(20).toString('hex'),'replyID':replyID,'uuID':auth_user.uuID,
        'quoteID':commentedQuote.quoteID,'addedAt':reply.addedAt,'avatarURI':auth_user.avatarURI,'myCommentID':req.body.commentID} //action -3 replied to comment 
        await User.updateOne({ 'uuID': whoPostedComment }, { $push: { notifications: notification } });

        await commentedQuote.save()
        let addedUser=await User.findOne({ 'uuID': whoPostedComment });
        var message = new gcm.Message({
          priority: 'high',
          contentAvailable: true,
          data: {
            key1: commentedQuote.quoteID,
          },
          notification: {
            title: "Transfigure",
            icon: "./playstore.png",
            body: `${userName} replied on your comment`
          }
        });
        sender.send(message, { registrationTokens: [addedUser.deviceHash] }, function (err, response) {
          if (err) console.error(err,"sdsdasdsdsdasdsd");
          else console.log(response,"responsenotification");
        });
        res.status(200).json({success:true})
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/like', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let userName=auth_user.firstName+' '+auth_user.lastName

        let likedQuote=await Quote.findOne({ 'quoteID': req.body.quoteID });
        if( !likedQuote.likedBy.includes(auth_user.uuID)){
          likedQuote.likedBy.push(auth_user.uuID);
          likedQuote.likes+=1;
        

          let notification={"action": 4, "name": userName,'notificationID':crypto.randomBytes(20).toString('hex'),'uuID':auth_user.uuID,
          'quoteID':likedQuote.quoteID,'addedAt':Date.now(),'avatarURI':auth_user.avatarURI} //action -4 liked quote
          await User.updateOne({ 'uuID': likedQuote.addedByUuid }, { $push: { notifications: notification } });
          await likedQuote.save()
          let addedUser=await User.findOne({ 'uuID': likedQuote.addedByUuid });
          var message = new gcm.Message({
            priority: 'high',
            contentAvailable: true,
            data: {
              key1: likedQuote.quoteID,
            },
            notification: {
              title: "Transfigure",
              icon: "./playstore.png",
              body: `${userName} liked your quote`
            }
          });
          sender.send(message, { registrationTokens: [addedUser.deviceHash] }, function (err, response) {
            if (err) console.error(err,"sdsdasdsdsdasdsd");
            else console.log(response,"responsenotification");
          });
          res.status(200).json({success:true})
        }
        else{
          res.status(300).json({success:false})
        }
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/dislike', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let userName=auth_user.firstName+' '+auth_user.lastName

        let likedQuote=await Quote.findOne({ 'quoteID': req.body.quoteID });
        if( likedQuote.likedBy.includes(auth_user.uuID)){
          
          
          
          likedQuote.likedBy.splice(likedQuote.likedBy.indexOf(auth_user.uuID) ,1);
          likedQuote.likes-=1;
          await likedQuote.save()
        res.status(200).json({success:true})
        }
        else{
          res.status(300).json({success:false})
        }
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/likeComment', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let userName=auth_user.firstName+' '+auth_user.lastName

        await Quote.updateOne({"quoteID":req.body.quoteID,"comments":{"$elemMatch":{"commentID":req.body.commentID}}}
        ,{"$inc":{"comments.$.likes":1},"$push":{"comments.$.likedBy":auth_user.uuID}});

        
        let likedQuote=await Quote.findOne({ 'quoteID': req.body.quoteID });
        let whoPostedComment=''
        for(let i=0;i<likedQuote.comments.length;i++){
          if(likedQuote.comments[i].commentID==req.body.commentID){
            whoPostedComment=likedQuote.comments[i].addedByUuid
          }
        }

        let notification={"action": 5, "name": userName,'notificationID':crypto.randomBytes(20).toString('hex'),'uuID':auth_user.uuID,
        'quoteID':likedQuote.quoteID,'addedAt':Date.now(),'avatarURI':auth_user.avatarURI,'myCommentID':req.body.commentID} //action -5 liked comment under quote
        await User.updateOne({ 'uuID': whoPostedComment }, { $push: { notifications: notification } });
        let addedUser=await User.findOne({ 'uuID': whoPostedComment });
        await likedQuote.save()
        var message = new gcm.Message({
          priority: 'high',
          contentAvailable: true,
          data: {
            key1: likedQuote.quoteID,
          },
          notification: {
            title: "Transfigure",
            icon: "./playstore.png",
            body: `${userName} liked your comment`
          }
        });
        sender.send(message, { registrationTokens: [addedUser.deviceHash] }, function (err, response) {
          if (err) console.error(err,"sdsdasdsdsdasdsd");
          else console.log(response,"responsenotification");
        });
        res.status(200).json({success:true})
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/dislikeComment', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let userName=auth_user.firstName+' '+auth_user.lastName

        let foundQuote=await Quote.findOne({"quoteID":req.body.quoteID})
        let oldComments=foundQuote.comments;
        for (let i=0;i<oldComments.length;i++){
          if(foundQuote.comments[i].commentID==req.body.commentID){
            let foundIndex=oldComments[i].likedBy.indexOf(auth_user.uuID);
            if(foundIndex!=-1){
              oldComments[i].likedBy.splice(foundIndex,1)
              oldComments[i].likes=oldComments[i].likes-1
            }
          }
        }

        
        await Quote.updateOne({"quoteID":req.body.quoteID},{"$set":{comments:oldComments}})

        res.status(200).json({success:true})
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/addtofavorites', async (req, res) => { 
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        if(auth_user.favoriteQuotes.includes(req.body.quoteID)){
          for(let i=0;i<auth_user.favoriteQuotes.length;i++){
            if(auth_user.favoriteQuotes[i]==req.body.quoteID){
              auth_user.favoriteQuotes.splice(i,1)
            }
          }
        }
        else{
          auth_user.favoriteQuotes.push(req.body.quoteID)
        }

        await auth_user.save()


        res.status(200).json({success:true})
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })

  function uploadImages (item,mykey) {
    let buf = Buffer.from(item.replace(/^data:image\/\w+;base64,/, ""),'base64')
    let data = {
      Key: mykey, 
      Body: buf,
      ContentEncoding: 'base64',
      ContentType: 'image/jpeg'
    };
    return new Promise((resolve) => {
      s3.putObject(data, async function(err, data){
        if (err) { 
          console.log(err);
          console.log('Error uploading data: ', data);
          resolve('')
        } else {
          console.log(data)
          // console.log(`succesfully uploaded the image at ${imageurl}`);
          resolve(`https://${process.env.AWS_BUCKETNAME}.s3.eu-west-2.amazonaws.com/${mykey}`)
        }
      });
    })
  }

  module.exports = router