
const tools = require('../src/emai');
const express = require('express')
const router = express.Router()
const crypto = require("crypto");
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SGAPI_KEY);
const User = require('../models/User')
const Quote = require('../models/Quote')




// Getting all
router.post('/', async (req, res) => {
  try {
    let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
      if(user){
      res.status(200).json({success:true,notifications:user.notifications})
      }
      else{
        res.status(400).json({success:false})
      }
    }
   catch (err) {
    res.status(500).json({ message: err.message })
  }
})


router.post('/getAllPosts', async (req, res) => { //fetch all posts notifications
  console.log(req.body.authKey,"req.body.authKey")
  try {
    let user = await User.findOne({'authKey':req.body.authKey},{notifications:1}) //my auth key
    if (user) {
        let newNotifications=[]
        let myObj=0
        let fetchedQuote=0
        console.log(user.notifications,"user.notifications")
        for(let i=0;i<user.notifications.length;i++){

          if(user.notifications[i].action==0){
            let checkuser=await User.findOne({'uuID':user.notifications[i].uuID},{username:1})
            if(checkuser){
              myObj={addedAt:user.notifications[i].addedAt,name:user.notifications[i].name,action:user.notifications[i].action,
              avatarURI:user.notifications[i].avatarURI,uuID:user.notifications[i].uuID,notificationID:user.notifications[i].notificationID}
              newNotifications.push(myObj)
            }
          }
          if(user.notifications[i].action==1 || user.notifications[i].action == 6){
            fetchedQuote=await Quote.findOne({'quoteID':user.notifications[i].quoteID},{likes:1,comments:1})
            if(fetchedQuote){
            myObj={addedAt:user.notifications[i].addedAt,name:user.notifications[i].name,action:user.notifications[i].action,
              avatarURI:user.notifications[i].avatarURI,title:user.notifications[i].title,notificationID:user.notifications[i].notificationID,quoteID:user.notifications[i].quoteID,
              likes:fetchedQuote.likes,comments:fetchedQuote.comments.length}
            newNotifications.push(myObj)
            }
          }
          if(user.notifications[i].action==3){ ///we need to show likes and comments under our comment
            fetchedQuote=await Quote.findOne({'quoteID':user.notifications[i].quoteID},{likes:1,comments:1})
            let likes=0
            let comments=0
            if(fetchedQuote){
              for(let p=0;p<fetchedQuote.comments.length;p++){ 
                if(fetchedQuote.comments[p].commentID ==user.notifications[i].myCommentID){
                  likes=fetchedQuote.comments[p].likes
                  comments=fetchedQuote.comments[p].replies.length
                } 
              }
           
            myObj={addedAt:user.notifications[i].addedAt,name:user.notifications[i].name,action:user.notifications[i].action,
              avatarURI:user.notifications[i].avatarURI,notificationID:user.notifications[i].notificationID,quoteID:user.notifications[i].quoteID,
              likes:likes,comments:comments}
            newNotifications.push(myObj)
            }
          }
          if(user.notifications[i].action==5){ ///someone liked our comment
            fetchedQuote=await Quote.findOne({'quoteID':user.notifications[i].quoteID},{likes:1,comments:1})
            let likes=0
            let comments=0
            if(fetchedQuote){
              for(let z=0;z<fetchedQuote.comments.length;z++){ 
                if(fetchedQuote.comments[z].commentID ==user.notifications[i].myCommentID){
                  likes=fetchedQuote.comments[z].likes
                  comments=fetchedQuote.comments[z].replies.length
                } 
              }
            
            myObj={addedAt:user.notifications[i].addedAt,name:user.notifications[i].name,action:user.notifications[i].action,
              avatarURI:user.notifications[i].avatarURI,notificationID:user.notifications[i].notificationID,quoteID:user.notifications[i].quoteID,
              likes:likes,comments:comments}
            newNotifications.push(myObj)
            }
          }
          if(user.notifications[i].action==7 || user.notifications[i].action == 8){ ///message send by admin
            myObj={addedAt:user.notifications[i].addedAt,name:user.notifications[i].name,action:user.notifications[i].action,
              notificationID:user.notifications[i].notificationID, title: user.notifications[i].title}
            newNotifications.push(myObj)
          }          
        }

      res.status(200).json({success:true,notifications:newNotifications})
      }
      else{
        res.status(400).json({success:false})
      }
    }
   catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getMyPosts', async (req, res) => { //fetch my posts notifications
  try {
    let user = await User.findOne({'authKey':req.body.authKey},{notifications:1}) //my auth key
      if(user){
        let newNotifications=[]
        let myObj=0
        let fetchedQuote=0
        for(let i=0;i<user.notifications.length;i++){

          if(user.notifications[i].action==2){//comment under our quote
            
            fetchedQuote=await Quote.findOne({'quoteID':user.notifications[i].quoteID},{likes:1,comments:1})
            if(fetchedQuote){
            myObj={addedAt:user.notifications[i].addedAt,name:user.notifications[i].name,action:user.notifications[i].action,
              avatarURI:user.notifications[i].avatarURI,title:user.notifications[i].title,notificationID:user.notifications[i].notificationID,quoteID:user.notifications[i].quoteID,
              likes:fetchedQuote.likes,comments:fetchedQuote.comments.length}
            newNotifications.push(myObj)
            }
          }
          if(user.notifications[i].action==4){ //liked our quote
            fetchedQuote=await Quote.findOne({'quoteID':user.notifications[i].quoteID},{likes:1,comments:1})
            if(fetchedQuote){           
            myObj={addedAt:user.notifications[i].addedAt,name:user.notifications[i].name,action:user.notifications[i].action,
              avatarURI:user.notifications[i].avatarURI,notificationID:user.notifications[i].notificationID,quoteID:user.notifications[i].quoteID,
              likes:fetchedQuote.likes,comments:fetchedQuote.comments.length}
            newNotifications.push(myObj)
              }
            }

          }

       
        

      res.status(200).json({success:true,notifications:newNotifications})
      }
      else{
        res.status(400).json({success:false})
      }
    }
   catch (err) {
    res.status(500).json({ message: err.message })
  }
})

router.post('/getlengtheverything', async (req, res) => { //fetch all posts notifications
  try {
    let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
      if(user){
        let alllength=0
        let founduser=0
        let foundquote=0
        let notificopy=user.notifications
        for(let z=0;z<user.notifications.length;z++){
          let hours = Math.abs(user.notifications[z].addedAt - Date.now()) / 36e5;
          if(user.notifications[z].action==0){
             founduser=await User.findOne({'uuID':user.notifications[z].uuID},{username:1}) //to fetch faster
            if(founduser){
              alllength+=1;
            }
            else{
              notificopy.splice(z,1)
            }
          }else if((user.notifications[z].action==7 && hours <= 24) || (user.notifications[z].action==8 && hours <= 24)) {
            founduser=await User.findOne({'uuID':user.notifications[z].uuID},{username:1}) //to fetch faster
            if(founduser){
              alllength+=1;
            }
            else{
              notificopy.splice(z,1)
            }
          }
          else{
            foundquote= await Quote.findOne({'quoteID':user.notifications[z].quoteID},{quoteText:1})
            if(foundquote){
              alllength+=1;
            }
            else{
              notificopy.splice(z,1)
            }
          }

        }
        user.notifications=notificopy
        await user.save()

      res.status(200).json({success:true,notificationslength:alllength})
      }
      else{
        res.status(400).json({success:false})
      }
    }
   catch (err) {
    res.status(500).json({ message: err.message })
  }
})

router.post('/delete', async (req, res) => {
    try {
      let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
      if(user){
          
      for (let i=0;i<user.notifications.length;i++){
          if(user.notifications[i].notificationID==req.body.notificationID){
            user.notifications.splice(i,1)
          }
      }
      await user.save()
      res.status(200).json({success:true,deleted:req.body.notificationID})
    }
    else{
      res.status(400).json({ success: false })
    }
      }
     catch (err) {
      res.status(500).json({ message: err.message })
    }
  })



module.exports = router