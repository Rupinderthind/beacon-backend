
const tools = require('../src/emai');
const express = require('express')
const router = express.Router()
const crypto = require("crypto");
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SGAPI_KEY );
const User = require('../models/User')





// Getting all
router.post('/', async (req, res) => {
    try {
      let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
      if(user){
        let newarray=user.friends
        for(let i=0;i<user.friends.length;i++){
          for(let p=0;p<user.friends.length;p++){
            if(user.friends[i].uuID==user.friends[p].uuID && i!=p){
              newarray.splice(i,1)
            }
          }
        }
        user.friends=newarray
        await user.save()
        res.status(200).json({ friends:newarray ,success:true })
      }
      else{
        res.status(400).json({ success:false })
      }
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/new', async (req, res) => {
    try {
      let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
      if(user){
        let friendsuuIDs=[]
        for(let i=0;i<user.friends.length;i++){
          if(user.friends[i].accepted==true){
          friendsuuIDs.push(user.friends[i].uuID)
          }
        }
        friendsuuIDs.push(user.uuID)
        let newFriends;
        if(req.body.searchWord!=''){
          let expression='.*'+req.body.searchWord+'.*'
          newFriends=await User.find({'uuID':{'$nin':friendsuuIDs},'confirmed':true,'$or': [{'firstName': {'$regex' : expression, '$options' : 'i'} }, {'lastName': {'$regex' : expression, '$options' : 'i'} } ] },{uuID:1,firstName:1,lastName:1,avatarURI:1}).skip(req.body.skip).limit(4)

        }
        else{
          newFriends=await User.find({'uuID':{'$nin':friendsuuIDs},'confirmed':true},{uuID:1,firstName:1,lastName:1,avatarURI:1}).skip(req.body.skip).limit(4)
        }
        res.status(200).json({ newFriends: newFriends,success:true })
      }
      else{
        res.status(400).json({ success:false })
      }
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
router.post('/add', async (req, res) => {
  try {
    let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
    let wasAlreadyRequested=false
    for (let i=0;i<user.friends.length;i++){
        if(user.friends[i].uuID==req.body.uuID){wasAlreadyRequested=true}
    }
    if(!wasAlreadyRequested){

        let addedFriend = await User.findOne({'uuID':req.body.uuID})

    
    let myName=user.firstName+' '+user.lastName
    let friendsName=addedFriend.firstName+' '+addedFriend.lastName
    user.friends.push({ "uuID": req.body.uuID,'accepted':false,'name':friendsName,'avatarURI':addedFriend.avatarURI}) //uuID of added friend 
    addedFriend.notifications.push({"action": 0, "uuID": user.uuID,'name':myName,'notificationID':crypto.randomBytes(20).toString('hex'),'addedAt':Date.now(),'avatarURI':user.avatarURI}) //action -0 friend request
     
    await user.save()
    await addedFriend.save()
    res.status(200).json({success:true})
    }
    else{
        res.status(400).json({success:false})
    }
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/accept', async (req, res) => { //send authKey,uuId from notification
    try {
         
      let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
      let userFriendRequested = await User.findOne({'uuID':req.body.frienduuID}) //my auth key
      let friendsName=userFriendRequested.firstName+' '+userFriendRequested.lastName


      user.friends.push({"name": friendsName, "uuID": req.body.frienduuID,'accepted':true,'avatarURI':userFriendRequested.avatarURI})
      let oldValue=''
      for(let i=0;i<userFriendRequested.friends.length;i++){ //add to my friends
        
        if(userFriendRequested.friends[i].uuID==user.uuID){
            oldValue=userFriendRequested.friends[i]
            oldValue.accepted=true
            userFriendRequested.friends.splice(i,1)
            
        }
    }
    userFriendRequested.friends.push(oldValue)
    ///from phone we need to request to clear notification
    await user.save()
    await userFriendRequested.save()
    res.status(200).json({success:true,accepted:friendsName})
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/decline', async (req, res) => { //send authKey,uuId from notification
    try {
         
      let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
      let userFriendRequested = await User.findOne({'uuID':req.body.frienduuID}) //my auth key

      for(let i=0;i<userFriendRequested.friends.length;i++){ //add to my friends
        
        if(userFriendRequested.friends[i].uuID==user.uuID){
            userFriendRequested.friends.splice(i,1)
        }
    }

    ///from phone we need to request to clear notification
    await userFriendRequested.save()
    res.status(200).json({success:true})
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
router.post('/delete', async (req, res) => { //send authKey,frienduuId from notification
    try {
         
      let user = await User.findOne({'authKey':req.body.authKey}) //my auth key
      if(user){
          let userToBeRemoved = await User.findOne({'uuID':req.body.frienduuID}) //my auth key
          for (let i=0;i<user.friends.length;i++){
              if(user.friends[i].uuID==req.body.frienduuID){user.friends.splice(i,1)}
          }
          for (let i=0;i<userToBeRemoved.friends.length;i++){
            if(userToBeRemoved.friends[i].uuID==user.uuID){userToBeRemoved.friends.splice(i,1)}
          }
          await user.save()
          await userToBeRemoved.save()
          res.status(200).json({ success: true })
      }
      else{res.status(400).json({ success: false })}

    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })






module.exports = router