const tools = require('../src/emai');
const express = require('express')
const router = express.Router()
const crypto = require("crypto");
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SGAPI_KEY);
const User = require('../models/User')
const Quote = require('../models/Quote')
const Report = require('../models/Report')




// Getting all 
router.post('/quote', async (req, res) => {
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
      let foundQuote=await Quote.findOne({'quoteID':req.body.quoteID})
        let report = new Report({
            reason: req.body.reason,
            quoteID: req.body.quoteID,
            reportedBy:auth_user.uuID,
            addedByUuid:foundQuote.addedByUuid
          })


        await report.save()
      res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/user', async (req, res) => {
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
        let report = new Report({
            reason: req.body.reason,
            userID: req.body.userID,
            reportedBy:auth_user.uuID
          })


        await report.save()
      res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/reflection', async (req, res) => {
  try {
      let auth_user= await User.findOne({ 'authKey': req.body.authKey})
    
    if(auth_user){
        let report = new Report({
            reason: req.body.reason,
            reflectionID: req.body.reflectionID,
            reportedBy:auth_user.uuID
          })


        await report.save()
      res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

  module.exports = router