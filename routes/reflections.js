const tools = require('../src/emai');
const express = require('express')
const router = express.Router()
const crypto = require("crypto");
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SGAPI_KEY);
const Admin = require('../models/Admin')
const Reflection = require('../models/Reflection')
const User = require('../models/User')
const AWS = require('aws-sdk');
AWS.config.update({region: 'us-west-2'});
const s3 = new AWS.S3({accessKeyId: process.env.AWS_ACCESKEYID ,secretAccessKey: process.env.AWS_SECRETKEY,params:{Bucket:process.env.AWS_BUCKETNAME}});


  router.post('/getReflection', async (req, res) => {
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let found_reflection=await Reflection.findOne({})
        if(found_reflection){
            res.status(200).json({success:true,reflection:found_reflection})

        }
        else{
            res.status(300).json({success:false})
        }
          
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/like', async (req, res) => {
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let found_reflection=await Reflection.findOne({})
        if(found_reflection){
            if(!found_reflection.likedBy.includes(auth_user.uuID)){
                found_reflection.likes+=1
                found_reflection.likedBy.push(auth_user.uuID)
                await found_reflection.save()
                res.status(200).json({success:true})
            }
            else{
                res.status(301).json({success:false})

            }
            
            

        }
        else{
            res.status(300).json({success:false})

        }
          
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/dislike', async (req, res) => {
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let found_reflection=await Reflection.findOne({})
        if(found_reflection){
            if(found_reflection.likedBy.includes(auth_user.uuID)){
                found_reflection.likes-=1
                found_reflection.likedBy.splice(found_reflection.likedBy.indexOf(auth_user.uuID) ,1)
                await found_reflection.save()
                res.status(200).json({success:true})
            }
            else{
                res.status(301).json({success:false})

            }
            
            

        }
        else{
            res.status(300).json({success:false})

        }
          
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })

  router.post('/addComment', async (req, res) => {
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let found_reflection=await Reflection.findOne({})

        if(found_reflection){

            let comID=crypto.randomBytes(20).toString('hex')
            let userName=auth_user.firstName+' '+auth_user.lastName
            let comment={
              commentText:req.body.commentText,
              commentID:comID,
              replies:[],
              addedBy:userName,
              addedByUuid:auth_user.uuID,
              addedAt:Date.now(),
              likes:0,
              repliesNumber:0,
              avatarURI:auth_user.avatarURI,
              likedBy:[]
            };
            found_reflection.comments.push(comment);
            await found_reflection.save()
            res.status(200).json({success:true})
        }
        else{
            res.status(300).json({success:false})

        }
          
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/replyToComment', async (req, res) => {
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let found_reflection=await Reflection.findOne({})
        if(found_reflection){

            let replyID=crypto.randomBytes(20).toString('hex')
            let userName=auth_user.firstName+' '+auth_user.lastName
            let reply={
              replyText:req.body.replyText,
              replyID:replyID,
              replies:[],
              addedBy:userName,
              addedAt:Date.now(),
              addedByUuid:auth_user.uuID,
              likes:0,
              repliesNumber:0,
              avatarURI:auth_user.avatarURI
            };
            await Reflection.updateOne({"comments":{"$elemMatch":{"commentID":req.body.commentID}}}
            ,{"$push":{"comments.$.replies":reply},"$inc":{"comments.$.repliesNumber":1}}); 

            res.status(200).json({success:true})
        }
        else{
            res.status(300).json({success:false})

        }
          
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })

  router.post('/likeComment', async (req, res) => {
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let found_reflection=await Reflection.findOne({})
        if(found_reflection){

            await Reflection.updateOne({"comments":{"$elemMatch":{"commentID":req.body.commentID}}}
            ,{"$inc":{"comments.$.likes":1},"$push":{"comments.$.likedBy":auth_user.uuID}});
            res.status(200).json({success:true})
        }
        else{
            res.status(300).json({success:false})

        }
          
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/dislikeComment', async (req, res) => {
    try {
        let auth_user= await User.findOne({ 'authKey': req.body.authKey})
      
      if(auth_user){
        let found_reflection=await Reflection.findOne({})
        if(found_reflection){

            let oldComments=found_reflection.comments;
            for (let i=0;i<oldComments.length;i++){
              if(found_reflection.comments[i].commentID==req.body.commentID){
                let foundIndex=oldComments[i].likedBy.indexOf(auth_user.uuID);
                if(foundIndex!=-1){
                  oldComments[i].likedBy.splice(foundIndex,1)
                  oldComments[i].likes=oldComments[i].likes-1
                }
              }
            }
    
            
            await Reflection.updateOne({},{"$set":{comments:oldComments}})

            res.status(200).json({success:true})
        }
        else{
            res.status(300).json({success:false})

        }
          
      }
      else{
        res.status(400).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })


module.exports = router