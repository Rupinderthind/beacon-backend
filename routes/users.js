
const tools = require('../src/emai');
const express = require('express')
const router = express.Router()
const crypto = require("crypto");
const sgMail = require('@sendgrid/mail');
const math = require("mathjs")
const { check, validationResult } = require('express-validator');
sgMail.setApiKey(process.env.SGAPI_KEY);
const User = require('../models/User')
const Quote = require('../models/Quote')
const Contact = require('../models/contactsupport')
const Report = require('../models/Report')
const Ban = require('../models/Ban')

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-west-2'});
const s3 = new AWS.S3({accessKeyId: process.env.AWS_ACCESKEYID,secretAccessKey: process.env.AWS_SECRETKEY,params:{Bucket:process.env.AWS_BUCKETNAME}});


//aws pass N1111111!



// Getting all
router.post('/', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
      const users = await User.find({ authKey: { $ne: req.body.authKey }})
      res.status(200).json(users)
    }
    else{
      res.status(400).json([])
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getOne', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
      let foundUser=await User.findOne({'uuID':req.body.userID},{firstName:1,lastName:1,displayTitle:1,uuID:1,friends:1,about:1,private:1,displayName:1})
      if(foundUser){  
        let foundImage=await User.findOne({'uuID':req.body.userID},{avatarURI:1})

        res.status(200).json({success:true,user:foundUser,avatar:foundImage.avatarURI})
      }
      else{
        res.status(300).json({success:false}) //user doest not exist
      }
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

router.post('/updateInfo', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
      let param=req.body.field
      let receivedObj=req.body.value
      if(typeof receivedObj === 'object'){
      if('imageChosen' in receivedObj){
        if(receivedObj.imageChosen){
          //save here
          if(receivedObj.imageBase64.length>500){
            let receivedImage=receivedObj.imageBase64

            let mykey=reqUser.uuID+'_mirrorreflection_'+crypto.randomBytes(20).toString('hex')
            buf = Buffer.from(receivedImage.replace(/^data:image\/\w+;base64,/, ""),'base64')
            let data = {
              Key: mykey, 
              Body: buf,
              ContentEncoding: 'base64', 
              ContentType: 'image/jpeg'
            };
            s3.putObject(data, async function(err, data){
                if (err) { 
                  console.log(err);
                  console.log('Error uploading data: ', data);
                  res.status(300).json({success:false}) 
                } else {
                  
                  receivedObj.imageBase64=`https://${process.env.AWS_BUCKETNAME}.s3.eu-west-2.amazonaws.com/${mykey}`
                  console.log(`succesfully uploaded the image at https://${process.env.AWS_BUCKETNAME}.s3.eu-west-2.amazonaws.com/${mykey}`);
                  reqUser[param]=receivedObj
                  await reqUser.save()
                  res.status(200).json({success:true})
                }
            });
        }
          else{
                    reqUser[param]=receivedObj
                    await reqUser.save()
                    res.status(200).json({success:true})
          }
        }
        else{
          reqUser[param]=receivedObj
          await reqUser.save()
          res.status(200).json({success:true})
        }
      }
      else{
        reqUser[param]=receivedObj
        await reqUser.save()
        res.status(200).json({success:true})
      }
    }
    else{
        reqUser[param]=receivedObj
        await reqUser.save()
        res.status(200).json({success:true})
    }
      

      

    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getSettingsInfo', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        let obj={username:reqUser.username,email:reqUser.email,countryLong:reqUser.countryLong,countryShort:reqUser.countryShort,timezone:reqUser.timezone,private:reqUser.private,defaultMirrorReflection:reqUser.defaultMirrorReflection,firstName:reqUser.firstName,avatarURI:reqUser.avatarURI}
        res.status(200).json({success:true,settings:obj})

    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

router.post('/setnotification', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
      reqUser.notificationEnabled = req.body.notificationEnabled
      reqUser.save().then(() => {
        res.status(200).json({success:true})
      })
    }
    else{
      res.status(400).json({success:false})
    }
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

router.post('/updatePassword', async (req, res) => { //reson 0- old password doesn't match
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        let old=req.body.oldPassword
        if(old==reqUser.password){
          reqUser.password=req.body.newPassword
          await reqUser.save()
          res.status(200).json({success:true})
        }
        else{
          res.status(300).json({success:false,reason:0})

        }

    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/deleteAccount', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){

      let friendObj=0
      for(let i=0;i<reqUser.friends.length;i++){
         friendObj=await User.findOne({'uuID':reqUser.friends[i].uuID})
        //delete from friend list of friend
        for(let j=0;j<friendObj.friends.length;j++){
          if(friendObj.friends[j].uuID==reqUser.uuID){
            friendObj.friends.splice(j,1)
          }
        }
        for(let p=0;p<friendObj.notifications.length;p++){
          if(friendObj.notifications[p].uuID==reqUser.uuID){
            friendObj.notifications.splice(p,1)
          }
        }
        await friendObj.save()
      }
      await Quote.deleteMany({ addedByUuid: reqUser.uuID }, async function(err, result) {
        if (err) {
          res.status(205).json({success:false})
        } else {


          let report = new Report({
            reason: req.body.reason,
            reportedBy:reqUser.uuID,
            deleteAccount:true
          })


          await report.save()
          await User.deleteOne({'uuID':reqUser.uuID})
          await Report.deleteMany({'userID':reqUser.uuID})
          res.status(200).json({success:true})
        }
      });


    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})




// Creating one
router.post('/register', async (req, res) => {




  let chosenuuid=crypto.randomBytes(20).toString('hex') //checking uuID of user
  let testeduser=await Quote.findOne({'uuID':chosenuuid})
  while(testeduser){
    chosenuuid=crypto.randomBytes(20).toString('hex')
    testeduser=await Quote.findOne({'uuID':chosenuuid})
  }

  let chosenauthkey=crypto.randomBytes(20).toString('hex') //checking authkey of user
  let testeduserkey=await Quote.findOne({'authKey':chosenauthkey})
  while(testeduserkey){
    chosenauthkey=crypto.randomBytes(20).toString('hex')
    testeduserkey=await Quote.findOne({'authKey':chosenauthkey})
  }
  let firs=tools.replaceEmoji(req.body.firstName)
  let las=tools.replaceEmoji(req.body.lastName)
  let usersemail=req.body.email.replace(' ','')
  if(firs!='' && las!=''){
  const user = new User({
    firstName: firs.replace(' ',''),
    lastName:  las.replace(' ',''),
    password: req.body.password,
    email: usersemail,
    confirmed:false,
    authKey:chosenauthkey,
    dateAdded:Date.now(),
    displayTitle:'',
    displayName:'',
    confirmationDate:0,
    friends:[],
    notifications:[],
    recentlySearched:[],
    favoriteQuotes:[],
    username:'',
    defaultMirrorReflection:{imageChosen:false,backgroundColor:'black',imageBase64:''},
    about:'',
    countryLong:'United Kingdom',
    countryShort:'GB',
    timezone:'Europe/London',
    private:false,
    resetPasswordCode:0,
    avatarURI:'https://thewanderers.travel/data_content/meet-the-wanderers/blank-user-img.jpg',
    uuID:chosenuuid,
    deviceHash: req.body.deviceHash
  })

   //do friends dodajemy uuID uzytkownika i jego nazwe 
 

  




  try {

    if(check(usersemail).isEmail() ){
      let resp_email=await User.findOne({'email':usersemail})
      let bannedEmail=await Ban.findOne({'email':usersemail})
      if(!resp_email && !bannedEmail){

        let msg = {
          to: usersemail,
          from: 'beaconformongo@gmail.com',
          subject: 'Email verification',
          text: 'message',
          html: tools.generateEmail(chosenauthkey) 
        };
        await sgMail.send(msg);
        await user.save()
        res.status(200).json({success:true})
        
        }
      else{//reason -0 email exist  -1 failed to get data from DB
        if(resp_email){
        res.status(400).json({success:false,reason:0} )
        }
        else{
          res.status(402).json({success:false,reason:3} )
        }
          }
        }

  } catch (err) {
    res.status(401).json({success:false,reason:1,message:err.message})
  }
}
else{
  res.status(403).json({success:false,reason:2})
}


})
router.get('/authemail/:id', async (req, res) => {
let token=req.params.id
try{
let resp_token=await User.findOne({'authKey':token})
if(resp_token){
  resp_token.confirmed=true
  resp_token.confirmationDate=Date.now()
  await resp_token.save()
  res.status(200).json('Thank you, please go back to app to login')
  }
  else{
    res.status(400).json('Something went wrong!')
  }
}
catch(err){console.log(err)}

})
router.post('/login', async (req, res) => {
  try{
    
  let resp=await User.findOne({'email':req.body.email,'password':req.body.password})
  console.log(resp,req.body.deviceHash,"resp login")
  if(resp){ //reason -0 not confirmed -1 wrong email/password
    // if(!resp.deviceHash) {
      resp.deviceHash = req.body.deviceHash
      await resp.save().then(res => {
        console.log(res,"res updated user")
      }).catch(err => {
        console.log('getting error')
      })
    // }
    if(resp.confirmed){res.status(200).json({success:true,authKey:resp.authKey,uuID:resp.uuID})}
    else{res.status(400).json({success:false,reason:0})}     
    }
    else{
      res.status(401).json({success:false,reason:1})
    }
  }
  catch(err){console.log(err)}
  
  })
  router.post('/sendResetEmail', async (req, res) => {
    try{
      
    let resp=await User.findOne({'email':req.body.email})
    
    if(resp){ 
      let code=math.floor(100000 + math.random() * 900000);
      const msg = {
        to: req.body.email,
        from: 'beaconformongo@gmail.com',
        subject: 'Password reset',
        text: 'message',
        html: tools.resetPassword(code) 
      };
      let rez=await sgMail.send(msg);
      resp.resetPasswordCode=code;
      await resp.save();
      res.status(200).json({success:true})
      }
      else{
        res.status(401).json({success:false,reason:0}) //0-wrong email -1 error while sending
      }
    }
    catch(err){
      res.status(400).json({success:false,reason:1})
    }
    
    })
    router.post('/checkCode', async (req, res) => {
      try{
        if(!isNaN(req.body.resetCode) ){
      let resp=await User.findOne({'resetPasswordCode':req.body.resetCode})
      
      if(resp){ 
          if(resp.email==req.body.resetEmail){
            res.status(200).json({success:true})
          }
          else{
            res.status(300).json({success:false,reason:2})
          }
        }
      else{
        res.status(401).json({success:false,reason:0}) //0-wrong email -1 error while sending -2 wrong code
      }
    }
    else{
      res.status(302).json({success:false,reason:3})
    }
      }
      catch(err){
        console.log(err)
        res.status(400).json({success:false,reason:1})
      }
      
      })

      router.post('/resetPassword', async (req, res) => {
        try{
        let resp= await User.updateOne({ 'email': req.body.resetEmail }, { $set: { password: req.body.resetPassword,resetPasswordCode:0 } });
        
        if(resp){ 
              res.status(200).json({success:true})
          }
        else{
          res.status(401).json({success:false,reason:0}) //0-wrong email or db failed -1 db failed
        }
        }
        catch(err){
          console.log(err)
          res.status(400).json({success:false,reason:1})
        }
        
        })
router.post('/getdisplayName', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        res.status(200).json({success:true,displayName:reqUser.displayName})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getdisplayTitle', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        res.status(200).json({success:true,displayTitle:reqUser.displayTitle})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getmyBio', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        res.status(200).json({success:true,about:reqUser.about})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})



router.post('/updatedisplayName', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        reqUser.displayName=req.body.displayName
        await reqUser.save()
        res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/updatedisplayTitle', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        reqUser.displayTitle=req.body.displayTitle
        await reqUser.save()
        res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/updatemyBio', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        reqUser.about=req.body.about
        await reqUser.save()
        res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/updateAvatar', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){
        let mykey=reqUser.uuID+'_avatar'+crypto.randomBytes(20).toString('hex')
        let userWithThisAvatarID=await User.findOne({'avatarURI':`https://${process.env.AWS_BUCKETNAME}.s3.eu-west-2.amazonaws.com/${mykey}`})
        while(userWithThisAvatarID){
          mykey=reqUser.uuID+'_avatar'+crypto.randomBytes(20).toString('hex')
          userWithThisAvatarID=await User.findOne({'avatarURI':`https://${process.env.AWS_BUCKETNAME}.s3.eu-west-2.amazonaws.com/${mykey}`})
        }
        
        buf = Buffer.from(req.body.base64Img.replace(/^data:image\/\w+;base64,/, ""),'base64')
        let data = {
          Key: mykey, 
          Body: buf,
          ContentEncoding: 'base64',
          ContentType: 'image/jpeg'
        };
        s3.putObject(data, async function(err, data){
            if (err) { 
              console.log(err);
              console.log('Error uploading data: ', data);
              res.status(300).json({success:false}) 
            } else {
              console.log(data)
              console.log(`succesfully uploaded the image at ${data.Location}`);
              reqUser.avatarURI=`https://${process.env.AWS_BUCKETNAME}.s3.eu-west-2.amazonaws.com/${mykey}`
              await reqUser.save()
              res.status(200).json({success:true})
            }
        });
        

    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/getAvatar', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){

        let foundImageUrl=await User.findOne({'uuID':req.body.useruuID},{avatarURI:1})
          res.status(200).json({success:true,avatar:foundImageUrl.avatarURI})

    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})
router.post('/contactus', async (req, res) => {
  try {
    let reqUser=await User.findOne({'authKey':req.body.authKey})
    if(reqUser){


      let msgToUser = {
        to: reqUser.email,
        from: 'beaconformongo@gmail.com',
        subject: 'You have contacted support!',
        text: 'message',
        html: tools.generateEmailToUser(reqUser.firstName,req.body.message),
      };
      await sgMail.send(msgToUser);


      let supmessage = new Contact({
        uuID: reqUser.uuID,
        email: reqUser.email,
        message: req.body.message,

      })
      await supmessage.save()
        res.status(200).json({success:true})
    }
    else{
      res.status(400).json({success:false})
    }
    
    
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

    
module.exports = router