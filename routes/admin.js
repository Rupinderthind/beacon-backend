const tools = require('../src/emai');
const express = require('express')
const router = express.Router()
const crypto = require("crypto");
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SGAPI_KEY);
const Admin = require('../models/Admin')
const Reflection = require('../models/Reflection')
const User = require('../models/User')
const Report = require('../models/Report')
const Quote = require('../models/Quote')
const Contact = require('../models/contactsupport')
const Ban = require('../models/Ban')


const AWS = require('aws-sdk');
AWS.config.update({region: 'us-west-2'});
const s3 = new AWS.S3({accessKeyId:process.env.AWS_ACCESKEYID,secretAccessKey:process.env.AWS_SECRETKEY,params:{Bucket:process.env.AWS_BUCKETNAME}});
router.post('/login', async (req, res) => {

    try {
        let admin= await Admin.findOne({ 'login': req.body.login,'password':req.body.password})
      if(admin){

        res.status(200).json({success:true,authKey:admin.authKey})          
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
router.post('/uploadReflection', async (req, res) => {
    try {
        let admin= await Admin.findOne({ 'authKey': req.body.authKey})
      
      if(admin){
          let oldref=await Reflection.findOne({})

          let newkey='myreflectionimage'+crypto.randomBytes(20).toString('hex')
          if(oldref!=null){
          await Reflection.deleteOne({})
          
          while(oldref.key==newkey){
            newkey='myreflectionimage'+crypto.randomBytes(20).toString('hex')
          }
        }
          
          let refle=new Reflection({
            comments:[],
            likes:0,
            likedBy:[],
            imageurl:"",
            reflectionText:req.body.reflectionText,
            fontColor:req.body.fontColor,
            dateAdded:Date.now(),
            key:newkey
          })

          let buf = Buffer.from(req.body.base64Img.replace(/^data:image\/\w+;base64,/, ""),'base64')
          let data = {
            Key: newkey, 
            Body: buf,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg'
          };
          if(oldref!=null){
          let params={   
          Key: oldref.key         
        };
          s3.deleteObject(params, async function(err, data){
            if (err) { 
              console.log(err);
              res.status(300).json({success:false}) 
            } else {
              console.log('deleted old reflection')
            }
        });
      }
        s3.putObject(data, async function(err, data){
          if (err) { 
            console.log(err);
            console.log('Error uploading data: ', data);
            res.status(300).json({success:false}) 
          } else {
            console.log(data)
            let imageurl=`https://${process.env.AWS_BUCKETNAME}.s3.eu-west-2.amazonaws.com/${newkey}`
            console.log(`succesfully uploaded the image at ${imageurl}`);
            refle.imageurl=imageurl
            await refle.save()
          res.status(200).json({success:true})
          }
      });
           

          
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(207).json({ message: err.message })
    }
  })
  router.post('/getAllUsers', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let users = await User.find({'confirmed':true},{firstName:1,lastName:1,uuID:1,friends:1,email:1})
        let connectedUsers=[]
        let obj=0
        let reports=0
        for(let i=0;i<users.length;i++){
          reports= await Report.find({'$or':[{'userID':users[i].uuID},{'addedByUuid':users[i].uuID},{'deleteAccount':true,'reportedBy':users[i].uuID}]})
          obj={firstName:users[i].firstName,lastName:users[i].lastName,uuID:users[i].uuID,friends:users[i].friends.length,reports:reports,email:users[i].email}
          connectedUsers.push(obj)
        }
        
        res.status(200).json({success:true,users:connectedUsers} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/banEmail', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let foundban=await Ban.findOne({'email':req.body.email})
        if(foundban){
          await foundban.remove()
        }
        else{
          let newban = new Ban({
            email: req.body.email
          })
          await newban.save()
        }
        
        
        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(207).json({ message: err.message })
    }
  })
  router.post('/getBanned', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let banned=await Ban.find({})
        let collectedUsers=[]
        let found=0
        for(let i=0;i<banned.length;i++){
          found=await User.find({'confirmed':true,'email':banned[i].email},{firstName:1,lastName:1,uuID:1,friends:1,email:1})
          if(found){
            collectedUsers.push(found)
          }
        }
              
        res.status(200).json({success:true,banned:collectedUsers} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  
  router.post('/getAllReports', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let reports = await Report.find({})
        let filteredReports=[]
        let found=''
        let mode=''
        let obj=''
        for(let i=0;i<reports.length;i++){ //filtering reports
          mode='' //quote
          if(reports[i].quoteID!=undefined){
            mode=0
          }
          if(reports[i].userID!=undefined){
            mode=1
          }
          if(reports[i].reflectionID!=undefined){
            mode=2
          }
          if(reports[i].deleteAccount==true){
            mode=3
          }
           found=0
          if(mode==0){
            found=await  Quote.findOne({'quoteID':reports[i].quoteID})
            if(found){
              console.log('we found our quote')
              obj={...found,reason:reports[i].reason,tag:'quote','id':reports[i]._id}
              filteredReports.push(obj)

            }
            else{
              await Report.deleteOne({'_id':reports[i]._id})
            }
          }
          if(mode==1){
            found=found=await  User.findOne({'uuID':reports[i].userID})
            if(found){
              obj={...found,reason:reports[i].reason,tag:'user','id':reports[i]._id}
              filteredReports.push(obj)

            }
            else{
              await Report.deleteOne({'_id':reports[i]._id})
            }
          }
          if(mode==2){
            found=await  Reflection.findOne({'_id':reports[i].reflectionID})
            if(found){
              obj={...found,reason:reports[i].reason,tag:'reflection','id':reports[i]._id}
              filteredReports.push(obj)
            }
            else{
              await Report.deleteOne({'_id':reports[i]._id})
            }
          }
          if(mode==3){

              obj={...reports[i],reason:reports[i].reason,tag:'deleteaccount','id':reports[i]._id}
              filteredReports.push(obj)
            
          }
        }
        
        res.status(200).json({success:true,reports:filteredReports} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/deleteReport', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        await Report.deleteOne({'_id':req.body.reportID})
        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/getSupportContacts', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let contacts=await Contact.find({})
        res.status(200).json({success:true,contacts:contacts} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/deleteContact', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        await Contact.deleteOne({'_id':req.body.contactID})
        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/respondToContact', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let msg = {
          to: req.body.userEmail,
          from: 'beaconformongo@gmail.com',
          subject: 'Message from support team-TRANSFIGURE',
          text:  'message',
          html: tools.generateBlankEmail(req.body.messageToUser)
        };
        await sgMail.send(msg);
        await Contact.deleteOne({'_id':req.body.contactID})
        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  
  router.post('/deleteQuote', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let foundQuote=await Quote.findOne({'quoteID':req.body.quoteID})

        let userwhoposted=await User.findOne({ 'uuID': foundQuote.addedByUuid},{notifications:1,friends:1})


        
      //we need to remove from friends notifications and my 
      //first remove this quote from my notifications
      for(let i=0;i<userwhoposted.notifications.length;i++){
        if('quoteID' in userwhoposted.notifications[i]){
          if(userwhoposted.notifications[i].quoteID==req.body.quoteID){
            userwhoposted.notifications.splice(i,1)
          }
        }
      }
      await userwhoposted.save()
      
      let foundfriend=0
      for(let p=0;p<userwhoposted.friends.length;p++){//scroll through all of user friends
        foundfriend=await User.findOne({ 'uuID': userwhoposted.friends[p].uuID},{notifications:1}) 
        if(foundfriend){//check if available
          for(let z=0;z<foundfriend.notifications.length;z++){
            if('quoteID' in foundfriend.notifications[z]){
              if(foundfriend.notifications[z].quoteID==req.body.quoteID){
                foundfriend.notifications.splice(z,1)
              }
            }
          }
          await foundfriend.save() 
        }
        
      
      }
      await Quote.deleteOne({'quoteID':req.body.quoteID})
        await Report.deleteMany({'quoteID':req.body.quoteID})
        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/deleteUser', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        //clear notifications of friends
        let usertobedeleted=await User.findOne({'uuID':req.body.userID})
        let friendObj=0
        for(let i=0;i<usertobedeleted.friends.length;i++){
           friendObj=await User.findOne({'uuID':usertobedeleted.friends[i].uuID})
          //delete from friend list of friend
          for(let j=0;j<friendObj.friends.length;j++){
            if(friendObj.friends[j].uuID==usertobedeleted.uuID){
              friendObj.friends.splice(j,1)
            }
          }
          for(let p=0;p<friendObj.notifications.length;p++){
            if(friendObj.notifications[p].uuID==usertobedeleted.uuID){
              friendObj.notifications.splice(p,1)
            }
          }
          await friendObj.save()
        }
        await Quote.deleteMany({ addedByUuid: usertobedeleted.uuID }, async function(err, result) {
          if (err) {
            res.status(205).json({success:false})
          } else {

            await User.deleteOne({'uuID':req.body.userID})
            await Report.deleteMany({'userID':req.body.userID})
            res.status(200).json({success:true})
          }
        });

      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(207).json({ message: err.message })
    }
  })
  router.post('/messageUser', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let foundReport=await Report.findOne({'_id':req.body.reportID})
        let mode=''
        if(foundReport.quoteID!=undefined){
          mode=0
        }
        if(foundReport.userID!=undefined){
          mode=1
        }
        if(foundReport.reflectionID!=undefined){
          mode=2
        }

        let email=''
        let founduser=''
        if(mode==0){
          let quote=await Quote.findOne({'quoteID':foundReport.quoteID}) 
           founduser=await User.findOne({'uuID':quote.addedByUuid})
          email=founduser.email
        }
        if(mode==1){
           founduser=await User.findOne({'uuID':foundReport.userID})
          email=founduser.email
        }



        let msg = {
          to: email,
          from: 'beaconformongo@gmail.com',
          subject: 'Message from support team-TRANSFIGURE',
          text:  'message',
          html: tools.generateBlankEmail(req.body.messageContent)
        };
        await sgMail.send(msg);

        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/messageUserUuid', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){

        let founduser=await User.findOne({'uuID':req.body.uuID})
        let email=founduser.email

        let msg = {
          to: email,
          from: 'beaconformongo@gmail.com',
          subject: 'Message from support team-TRANSFIGURE',
          text:  'message',
          html: tools.generateBlankEmail(req.body.messageContent)
        };
        await sgMail.send(msg);

        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/getAllUserQuotes', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let quotes=await Quote.find({'addedByUuid':req.body.uuID})
        
        res.status(200).json({success:true,quotes:quotes} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  router.post('/sendMessageToAll', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let users = await User.find({'confirmed':true},{email:1})
        let msg=''
        for(let i=0;i<users.length;i++){
           msg = {
            to: users[i].email,
            from: 'beaconformongo@gmail.com',
            subject: 'Message from support team-TRANSFIGURE',
            text:  'message',
            html: tools.generateBlankEmail(req.body.message)
          };
          await sgMail.send(msg);
        }
        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })

  router.post('/sendMessageToAllUsers', async (req, res) => {
    try {
      let reqUser=await Admin.findOne({'authKey':req.body.authKey})
      if(reqUser){
        let users = await User.find({'confirmed':true},{email:1, uuID: 1})
        if(users.length) {
          users.forEach(async user => {
            console.log(user,"user.uuID")
            let notification={"action": 7, "name": "Team Transfigure",'title':req.body.message,
                'addedAt':Date.now(),'uuID':user.uuID }
            await User.updateOne({_id: user._id}, { $push: { notifications: notification } })
          })
        }
        res.status(200).json({success:true} )
      }
      else{
        res.status(203).json({success:false})
      }
      
      
    } catch (err) {
      res.status(500).json({ message: err.message })
    }
  })
  
  module.exports = router