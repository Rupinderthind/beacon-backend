
module.exports = {
    generateEmail: function (token) {
            let url='https://transfigureapp.herokuapp.com/users/authemail/'+token
            let html1='<p>Welcome! Thanks for signing up. Please follow this link to activate your account:</p><br>'
            let html2='<p><a href="'+url+'">Activate</a></p><br>'
            let html3='<p>Cheers!</p>'
            return html1+html2+html3    
    },
    resetPassword: function (code) {
        let html1="<p>Here's your verification code.Please enter it in the TRANSFIGURE App.</p><br>"
        let html2='<p>'+code+'</p><br>'
        let html3='<p>Cheers!</p>'
        return html1+html2+html3    
},
generateEmailToSupport: function (uuID,message) {
        let html=`<p>User ${uuID} wrote to support.</p><br> <p>Message content : ${message} </p>`
        return html   
},
generateEmailToUser: function (firstName,message) {
        let html=`<p>Dear ${firstName}!You wrote to support.</p><br> <p>Message content : ${message} </p><br><p>We will contact you as soon as possible</p><br><p>Cheers!</p>`
        return html   
},
replaceEmoji: function (mystring) {
        let removed=mystring.replace(/([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g, '');
        return removed   
},
generateBlankEmail: function (message) {
        let html1='<p>Hello!</p><p>Your received message from TRANSFIGURE support team</p>'
        let html2=`<p>${message}</p>`
        let html3='<p>Respond to this email to contact support team</p>'
        return html1+html2+html3   
},



  };