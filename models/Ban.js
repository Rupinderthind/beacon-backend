const mongoose = require('mongoose')
const crypto = require("crypto");

const banSchema = new mongoose.Schema({

  email: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('Bans', banSchema,'Bans')
