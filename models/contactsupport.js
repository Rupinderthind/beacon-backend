const mongoose = require('mongoose')

const contactSchema = new mongoose.Schema({
  uuID: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  message: {
    type: String,
    required: true
  },

})

module.exports = mongoose.model('ContactSupport', contactSchema,'ContactSupport')

