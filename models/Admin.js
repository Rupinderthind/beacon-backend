const mongoose = require('mongoose')
const crypto = require("crypto");

const adminSchema = new mongoose.Schema({

  login: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  authKey: {
    type: String,
    required: false
  }
})

module.exports = mongoose.model('AdminPanel', adminSchema,'AdminPanel')
