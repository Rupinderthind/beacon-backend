const mongoose = require('mongoose')
const crypto = require("crypto");

const reflectionSchema = new mongoose.Schema({

  comments: {
    type: Array,
    required: false
  },
  likes: {
    type: Number,
    required: false
  },
  likedBy:{
    type:Array,
    required:false
  },
  imageurl: {
    type: String,
    required: true
  },
  key: {
    type: String,
    required: true
  },
  reflectionText: {
    type: String,
    required: false,
  },
  fontColor: {
    type: String,
    required: false,
  },
  
  dateAdded: {
    type: Date,
    required: false,
  }

})

module.exports = mongoose.model('Reflections', reflectionSchema,'Reflections')

