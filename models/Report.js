const mongoose = require('mongoose')

const reportSchema = new mongoose.Schema({
  reason: {
    type: String,
    required: true
  },
  quoteID: {
    type: String,
    required: false
  },
  userID: {
    type: String,
    required: false
  },
  addedByUuid: {
    type: String,
    required: false
  },
  reflectionID: {
    type: String,
    required: false
  },
  
  reportedBy:{
      type: String,
      required: true
  },
  deleteAccount:{
    type: Boolean,
    required: false
}
 
})

module.exports = mongoose.model('Reports', reportSchema,'Reports')