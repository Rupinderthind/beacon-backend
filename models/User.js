const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  displayTitle: {
    type: String,
    required: false
  },
  uuID: {
    type: String,
    required: false
  },
  email: {
    type: String,
    required: true
  },  
  defaultMirrorReflection: {
      type: Object,
      required: true
    },
  recentlySearched: {
    type: Array,
    required: true
  },
  favoriteQuotes: {
    type: Array,
    required: true
  },
  
  confirmed: {
    type: Boolean,
    required: false,
  },
  
  username: {
    type: String,
    required: false,
  },
  countryLong: {
    type: String,
    required: false,
  },
  countryShort: {
    type: String,
    required: false,
  },
  timezone: {
    type: String,
    required: false,
  },
  about: {
    type: String,
    required: false,
  },
  displayName: {
    type: String,
    required: false,
  },
  private: {
    type: Boolean,
    required: false,
  },
  resetPasswordCode: {
    type: Number,
    required: false,
  },
  authKey: {
    type: String,
    required: false,
  },
  confirmationDate: {
    type: Date,
    required: false,

  },
  dateAdded: {
    type: Date,
    required: false,

  },
  avatarURI: {
    type: String,
    required: false,
  },
  friends: {
    type: Array,
    required: false,

  }
  ,
  notifications: {
    type: Array,
    required: false,
  },
  notificationEnabled: {
    type: Boolean,
    default: true,
    required: false
  },
  deviceHash: {
    type: String,
    required: false
  }
})

module.exports = mongoose.model('Users', userSchema,'Users')

