const mongoose = require('mongoose')
const crypto = require("crypto");
const { boolean } = require('mathjs');

const quoteSchema = new mongoose.Schema({
  addedByFirstName: {
    type: String,
    required: true
  },
  addedByLastName: {
    type: String,
    required: true
  },
  addedByUuid: {
    type: String,
    required: true
  },
  quoteID: {
    type: String,
    required: true
  },
  comments: {
    type: Array,
    required: false
  },
  userFriendList: {
    type: Array,
    required: true
  },
  likes: {
    type: Number,
    required: false
  },
  likedBy:{
    type:Array,
    required:false
  },
  title: {
    type: String,
    required: false
  },
  imageURI: {
    type: Array,
    required: false
  },
  imageChosen: {
    type: Boolean,
    required: false
  },
  backgroundColor: {
    type: String,
    required: false
  },
  style: {
    type: Object,
    required: true
  },
  quoteText: {
    type: String,
    required: false,
  },
  dateAdded: {
    type: Date,
    required: false,
  }
  ,
  visibleBy: { // 0 everyone,1 friends then check friend list //for me 
    type: Number,
    required: true
  },
  isQuote: {
    type: Boolean,
    required: false,
    default: false
  }

})

module.exports = mongoose.model('Quotes', quoteSchema,'Quotes')

